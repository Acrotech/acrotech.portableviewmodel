﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.ComponentModel;

namespace Acrotech.PortableViewModel.Test
{
    [TestClass]
    public class UnitTestViewModelTest
    {
        [TestMethod]
        public void ConstructorTest()
        {
            var vm = new UnitTestViewModel();

            Assert.IsFalse(vm.OnTextChangedOccurred);
            Assert.IsNull(vm.PreviousTextValue);
            Assert.IsNull(vm.Text);
        }

        [TestMethod]
        public void OnTextChangedTest()
        {
            var vm = new UnitTestViewModel();

            vm.ResetOnTextChanged();
            vm.Text = "test1";

            Assert.IsTrue(vm.OnTextChangedOccurred);
            Assert.IsNull(vm.PreviousTextValue);

            vm.ResetOnTextChanged();
            vm.Text = "test2";

            Assert.IsTrue(vm.OnTextChangedOccurred);
            Assert.AreEqual("test1", vm.PreviousTextValue);
        }

        [TestMethod]
        public void ResetTest()
        {
            var vm = new UnitTestViewModel();
            vm.Text = "test1";
            vm.Text = "test2";

            Assert.AreEqual("test1", vm.PreviousTextValue);

            vm.Reset();

            Assert.IsNull(vm.PreviousTextValue);
        }

        [TestMethod]
        public void ResetOnTextChangedTest()
        {
            var vm = new UnitTestViewModel();
            vm.Text = "test";

            Assert.IsTrue(vm.OnTextChangedOccurred);

            vm.ResetOnTextChanged();

            Assert.IsFalse(vm.OnTextChangedOccurred);
        }

        [TestMethod]
        public void TestOnPropertyChangedTest()
        {
            var vm = new UnitTestViewModel();

            var handlerCalled = false;
            string propertyName = null;
            var handler = new PropertyChangedEventHandler((s, e) => { handlerCalled = true; propertyName = e.PropertyName; });

            vm.PropertyChanged += handler;

            handlerCalled = false;
            propertyName = null;
            vm.TestOnPropertyChanged();
            Assert.IsTrue(handlerCalled);
            Assert.AreEqual("TestOnPropertyChangedTest", propertyName);

            handlerCalled = false;
            propertyName = null;
            vm.TestOnPropertyChanged("test");
            Assert.IsTrue(handlerCalled);
            Assert.AreEqual("test", propertyName);

            vm.PropertyChanged -= handler;
        }

        [TestMethod]
        public void TestRaiseAndSetIfChangedTest()
        {
            var vm = new UnitTestViewModel();

            var handlerCalled = false;
            string backingField = null;
            string result = null;

            var handler = new PropertyChangedEventHandler((s, e) => handlerCalled = true);

            vm.PropertyChanged += handler;

            handlerCalled = false;
            backingField = null;
            result = null;
            result = vm.TestRaiseAndSetIfChanged(ref backingField, "test");
            Assert.IsTrue(handlerCalled);
            Assert.AreEqual("test", result);
            Assert.AreEqual("test", backingField);

            handlerCalled = false;
            backingField = null;
            result = null;
            result = vm.TestRaiseAndSetIfChanged(ref backingField, "test", "Field");
            Assert.IsTrue(handlerCalled);
            Assert.AreEqual("test", result);
            Assert.AreEqual("test", backingField);

            handlerCalled = false;
            backingField = null;
            result = null;
            result = vm.TestRaiseAndSetIfChanged(ref backingField, "test", new PropertyChangedEventArgs("Field"));
            Assert.IsTrue(handlerCalled);
            Assert.AreEqual("test", result);
            Assert.AreEqual("test", backingField);

            handlerCalled = false;
            backingField = null;
            result = null;
            result = vm.TestRaiseAndSetIfChanged(ref backingField, "test", () => new PropertyChangedEventArgs("Field"));
            Assert.IsTrue(handlerCalled);
            Assert.AreEqual("test", result);
            Assert.AreEqual("test", backingField);

            vm.PropertyChanged -= handler;
        }
    }
}
