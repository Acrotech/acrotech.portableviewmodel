﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Threading.Tasks;
using System.Threading;
using System.Diagnostics.CodeAnalysis;
using System.ComponentModel;

namespace Acrotech.PortableViewModel.Test
{
    [TestClass]
    public class TaskBaseViewModelTest
    {
        [TestMethod]
        public void EmptyCompletedTaskTest()
        {
            Assert.IsNotNull(TaskBaseViewModel.EmptyCompletedTask);
            Assert.IsTrue(TaskBaseViewModel.EmptyCompletedTask.IsCompleted);
        }

        [TestMethod]
        [ExpectedException(typeof(InvalidOperationException))]
        [ExcludeFromCodeCoverage]
        public void InvalidSynchronizationContextConstructorTest()
        {
            TaskBaseViewModel.DefaultUITaskScheduler = null;
            SynchronizationContext.SetSynchronizationContext(null);

            new UnitTestTaskViewModel();
        }

        [TestMethod]
        public void ConstructorTest()
        {
            TaskBaseViewModel.DefaultUITaskScheduler = null;
            SynchronizationContext.SetSynchronizationContext(new SynchronizationContext());

            var s1 = TaskScheduler.FromCurrentSynchronizationContext();
            var s2 = TaskScheduler.FromCurrentSynchronizationContext();
            
            var vm = new UnitTestTaskViewModel();
            Assert.IsNotNull(vm.UIScheduler);

            var scheduler = TaskScheduler.Default;
            TaskBaseViewModel.DefaultUITaskScheduler = scheduler;
            vm = new UnitTestTaskViewModel();
            Assert.AreEqual(scheduler, vm.UIScheduler);

            TaskBaseViewModel.DefaultUITaskScheduler = null;
            vm = new UnitTestTaskViewModel(scheduler);
            Assert.AreEqual(scheduler, vm.UIScheduler);

            scheduler = null;
            vm = new UnitTestTaskViewModel(scheduler);
            Assert.AreEqual(scheduler, vm.UIScheduler);
        }

        [TestMethod]
        public void CreateEmptyCompletedTaskTest()
        {
            var task1 = TaskBaseViewModel.CreateEmptyCompletedTask<string>();
            Assert.IsNotNull(task1);
            Assert.IsTrue(task1.IsCompleted);
            Assert.AreEqual(default(string), task1.Result);

            task1 = TaskBaseViewModel.CreateEmptyCompletedTask<string>("test");
            Assert.IsNotNull(task1);
            Assert.IsTrue(task1.IsCompleted);
            Assert.AreEqual("test", task1.Result);

            var task2 = TaskBaseViewModel.CreateEmptyCompletedTask<int>();
            Assert.IsNotNull(task2);
            Assert.IsTrue(task2.IsCompleted);
            Assert.AreEqual(default(int), task2.Result);

            task2 = TaskBaseViewModel.CreateEmptyCompletedTask<int>(123);
            Assert.IsNotNull(task2);
            Assert.IsTrue(task2.IsCompleted);
            Assert.AreEqual(123, task2.Result);
        }

        [TestMethod]
        [ExpectedException(typeof(InvalidOperationException))]
        [ExcludeFromCodeCoverage]
        public void CheckForNullUISchedulerTest()
        {
            var vm = new UnitTestTaskViewModel(null);
            var po = new PrivateObject(vm, new PrivateType(typeof(TaskBaseViewModel)));

            po.Invoke("CheckForNullUIScheduler");
        }

        [TestMethod]
        public void CheckForNullUISchedulerWithValidSchedulerTest()
        {
            var vm = new UnitTestTaskViewModel(TaskScheduler.Default);
            var po = new PrivateObject(vm, new PrivateType(typeof(TaskBaseViewModel)));

            // ensure no exceptions occur
            po.Invoke("CheckForNullUIScheduler");
        }

        [TestMethod]
        [ExpectedException(typeof(InvalidOperationException))]
        [ExcludeFromCodeCoverage]
        public void RunOnUiThreadWithoutSchedulerWithoutResultTest()
        {
            var vm = new UnitTestTaskViewModel(null);

            vm.TestRunOnUiThread((Action)null);
        }

        [TestMethod]
        [ExpectedException(typeof(InvalidOperationException))]
        [ExcludeFromCodeCoverage]
        public void RunOnUiThreadWithoutSchedulerWithResultTest()
        {
            var vm = new UnitTestTaskViewModel(null);

            vm.TestRunOnUiThread((Func<string>)null);
        }

        [TestMethod]
        public void RunOnUiThreadTest()
        {
            SynchronizationContext.SetSynchronizationContext(new SynchronizationContext());
            var uiScheduler = TaskScheduler.FromCurrentSynchronizationContext();
            var vm = new UnitTestTaskViewModel(uiScheduler);

            var actionCalled = false;
            TaskScheduler scheduler = null;

            var task1 = vm.TestRunOnUiThread(() => { actionCalled = true; scheduler = TaskScheduler.Current; });
            Assert.IsNotNull(task1);

            task1.Wait();
            Assert.IsTrue(task1.IsCompleted);
            Assert.IsTrue(actionCalled);
            Assert.AreEqual(uiScheduler, scheduler);

            actionCalled = false;
            scheduler = null;
            var task2 = vm.TestRunOnUiThread(() => { actionCalled = true; scheduler = TaskScheduler.Current; return "test"; });
            Assert.IsNotNull(task2);

            Assert.AreEqual("test", task2.Result);
            Assert.IsTrue(task2.IsCompleted);
            Assert.IsTrue(actionCalled);
            Assert.AreEqual(uiScheduler, scheduler);
        }

        [TestMethod]
        public void RunOffUiThreadTest()
        {
            SynchronizationContext.SetSynchronizationContext(new SynchronizationContext());
            var uiScheduler = TaskScheduler.FromCurrentSynchronizationContext();
            var vm = new UnitTestTaskViewModel(uiScheduler);

            var actionCalled = false;
            TaskScheduler scheduler = null;

            var task1 = vm.TestRunOffUiThread(() => { actionCalled = true; scheduler = TaskScheduler.Current; });
            Assert.IsNotNull(task1);

            task1.Wait();
            Assert.IsTrue(task1.IsCompleted);
            Assert.IsTrue(actionCalled);
            Assert.AreNotEqual(uiScheduler, scheduler);

            actionCalled = false;
            scheduler = null;
            var task2 = vm.TestRunOffUiThread(() => { actionCalled = true; scheduler = TaskScheduler.Current; return "test"; });
            Assert.IsNotNull(task2);

            Assert.AreEqual("test", task2.Result);
            Assert.IsTrue(task2.IsCompleted);
            Assert.IsTrue(actionCalled);
            Assert.AreNotEqual(uiScheduler, scheduler);
        }

        [TestMethod]
        public void StartTaskWithoutResultTest()
        {
            SynchronizationContext.SetSynchronizationContext(new SynchronizationContext());
            var uiScheduler = TaskScheduler.FromCurrentSynchronizationContext();
            var actionCalled = false;
            TaskScheduler scheduler = null;
            Action action = () => { actionCalled = true; scheduler = TaskScheduler.Current; };

            var task = TaskBaseViewModel.StartTask(null);
            Assert.IsNotNull(task);
            Assert.IsTrue(task.IsCompleted);

            actionCalled = false;
            scheduler = null;
            task = TaskBaseViewModel.StartTask(action);
            Assert.IsNotNull(task);
            task.Wait();
            Assert.IsTrue(actionCalled);
            Assert.AreEqual(TaskScheduler.Default, scheduler);

            actionCalled = false;
            scheduler = null;
            task = TaskBaseViewModel.StartTask(action, scheduler: uiScheduler);
            Assert.IsNotNull(task);
            task.Wait();
            Assert.IsTrue(actionCalled);
            Assert.AreEqual(uiScheduler, scheduler);
        }

        [ExcludeFromCodeCoverage]
        private void StartTaskNoResult()
        {
        }

        [TestMethod]
        [ExpectedException(typeof(TaskCanceledException))]
        [ExcludeFromCodeCoverage]
        public void StartTaskWithoutResultTaskCancellationTest()
        {
            var cancelSource = new CancellationTokenSource();
            cancelSource.Cancel();
            var task = TaskBaseViewModel.StartTask(StartTaskNoResult, cancelSource.Token);
            try
            {
                task.Wait();
                Assert.Fail("Cancellation Failed");
            }
            catch (AggregateException ae)
            {
                Assert.IsTrue(task.IsCanceled);

                throw ae.Flatten().InnerException;
            }
        }

        [TestMethod]
        [ExpectedException(typeof(TaskCanceledException))]
        [ExcludeFromCodeCoverage]
        public void StartTaskWithoutResultDelgateCancellationTest()
        {
            var cancelSource = new CancellationTokenSource();
            var sim = new SimulatedLongRunningTask(cancelSource);
            var task = TaskBaseViewModel.StartTask(sim.Simulate, cancelSource.Token);
            cancelSource.CancelAfter(50);
            try
            {
                task.Wait();
                Assert.Fail("Cancellation Failed");
            }
            catch (AggregateException ae)
            {
                Assert.IsTrue(task.IsCanceled);
                Assert.IsFalse(sim.DelayExpired);

                throw ae.Flatten().InnerException;
            }
        }

        [TestMethod]
        [ExpectedException(typeof(TaskCanceledException))]
        [ExcludeFromCodeCoverage]
        public void StartTaskWithoutResultDelayCancellationTest()
        {
            var cancelSource = new CancellationTokenSource();
            var task = TaskBaseViewModel.StartTask(StartTaskNoResult, cancelSource.Token, delay: 1000);
            cancelSource.CancelAfter(50);
            try
            {
                task.Wait();
                Assert.Fail("Cancellation Failed");
            }
            catch (AggregateException ae)
            {
                Assert.IsTrue(task.IsCanceled);

                throw ae.Flatten().InnerException;
            }
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentOutOfRangeException))]
        [ExcludeFromCodeCoverage]
        public void StartTaskWithoutResultWithInvalidDelayTest()
        {
            TaskBaseViewModel.StartTask(StartTaskNoResult, delay: -2);
        }

        [TestMethod]
        public void StartTaskWithResultTest()
        {
            SynchronizationContext.SetSynchronizationContext(new SynchronizationContext());
            var uiScheduler = TaskScheduler.FromCurrentSynchronizationContext();
            var actionCalled = false;
            TaskScheduler scheduler = null;
            Func<string> action = () => { actionCalled = true; scheduler = TaskScheduler.Current; return "test"; };

            var task = TaskBaseViewModel.StartTask<string>(null);
            Assert.IsNotNull(task);
            Assert.IsTrue(task.IsCompleted);
            Assert.AreEqual(default(string), task.Result);

            actionCalled = false;
            scheduler = null;
            task = TaskBaseViewModel.StartTask(action);
            Assert.IsNotNull(task);
            task.Wait();
            Assert.IsTrue(actionCalled);
            Assert.AreEqual("test", task.Result);
            Assert.AreEqual(TaskScheduler.Default, scheduler);

            actionCalled = false;
            scheduler = null;
            task = TaskBaseViewModel.StartTask(action, scheduler: uiScheduler);
            Assert.IsNotNull(task);
            task.Wait();
            Assert.IsTrue(actionCalled);
            Assert.AreEqual("test", task.Result);
            Assert.AreEqual(uiScheduler, scheduler);
        }

        [ExcludeFromCodeCoverage]
        private string StartTaskReturnString()
        {
            return "test";
        }

        [ExcludeFromCodeCoverage]
        private string StartTaskCancelReturnString(CancellationTokenSource cancel)
        {
            cancel.Cancel(); cancel.Token.ThrowIfCancellationRequested(); return "test";
        }

        [TestMethod]
        [ExpectedException(typeof(TaskCanceledException))]
        [ExcludeFromCodeCoverage]
        public void StartTaskWithResultTaskCancellationTest()
        {
            var cancelSource = new CancellationTokenSource();
            cancelSource.Cancel();
            var task = TaskBaseViewModel.StartTask((Func<string>)StartTaskReturnString, cancelSource.Token);
            try
            {
                task.Wait();
                Assert.Fail("Cancellation Failed");
            }
            catch (AggregateException ae)
            {
                Assert.IsTrue(task.IsCanceled);

                throw ae.Flatten().InnerException;
            }
        }

        [TestMethod]
        [ExpectedException(typeof(TaskCanceledException))]
        [ExcludeFromCodeCoverage]
        public void StartTaskWithResultDelgateCancellationTest()
        {
            var cancelSource = new CancellationTokenSource();
            var sim = new SimulatedLongRunningTask(cancelSource);
            var task = TaskBaseViewModel.StartTask((Func<string>)sim.SimulateResult, cancelSource.Token);
            cancelSource.CancelAfter(50);
            try
            {
                task.Wait();
                Assert.Fail("Cancellation Failed");
            }
            catch (AggregateException ae)
            {
                Assert.IsTrue(task.IsCanceled);
                Assert.IsFalse(sim.DelayExpired);

                throw ae.Flatten().InnerException;
            }
        }

        [TestMethod]
        [ExpectedException(typeof(TaskCanceledException))]
        [ExcludeFromCodeCoverage]
        public void StartTaskWithResultDelayCancellationTest()
        {
            var cancelSource = new CancellationTokenSource();
            var task = TaskBaseViewModel.StartTask((Func<string>)StartTaskReturnString, cancelSource.Token, delay: 1000);
            cancelSource.CancelAfter(50);
            try
            {
                task.Wait();
                Assert.Fail("Cancellation Failed");
            }
            catch (AggregateException ae)
            {
                Assert.IsTrue(task.IsCanceled);

                throw ae.Flatten().InnerException;
            }
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentOutOfRangeException))]
        [ExcludeFromCodeCoverage]
        public void StartTaskWithResultWithInvalidDelayTest()
        {
            TaskBaseViewModel.StartTask((Func<string>)StartTaskReturnString, delay: -2);
        }

        [TestMethod]
        public void OnPropertyChangedTest()
        {
            var vm = new UnitTestTaskViewModel(TaskScheduler.Default);
            var po = new PrivateObject(vm, new PrivateType(typeof(TaskBaseViewModel)));

            ManualResetEventSlim wh = new ManualResetEventSlim();

            PropertyChangedEventHandler handler = new PropertyChangedEventHandler((s, e) => { wh.Set(); });
            Func<PropertyChangedEventArgs> argsCreator = () => { return null; };

            po.Invoke("OnPropertyChanged", handler, argsCreator);

            wh.Wait(1000);

            Assert.IsTrue(wh.IsSet);
        }

        [TestMethod]
        public void OnUiPropertyChangedTest()
        {
            SynchronizationContext.SetSynchronizationContext(new SynchronizationContext());
            var uiScheduler = TaskScheduler.FromCurrentSynchronizationContext();
            var vm = new UnitTestTaskViewModel(uiScheduler);
            var po = new PrivateObject(vm, new PrivateType(typeof(TaskBaseViewModel)));

            var handlerCalled = false;
            TaskScheduler scheduler = null;
            PropertyChangedEventHandler handler = new PropertyChangedEventHandler((s, e) => { handlerCalled = true; scheduler = TaskScheduler.Current; });
            Func<PropertyChangedEventArgs> argsCreator = () => { return null; };

            var task = (Task)po.Invoke("OnUiPropertyChanged", handler, argsCreator);
            Assert.IsNotNull(task);

            task.Wait();
            Assert.IsTrue(task.IsCompleted);
            Assert.AreNotEqual(TaskBaseViewModel.EmptyCompletedTask, task);
            Assert.IsTrue(handlerCalled);
            Assert.AreEqual(uiScheduler, scheduler);

            task = (Task)po.Invoke("OnUiPropertyChanged", new Type[] { typeof(PropertyChangedEventHandler), typeof(Func<PropertyChangedEventArgs>) }, new object[] { null, null });
            Assert.IsNotNull(task);

            task.Wait();
            Assert.AreEqual(TaskBaseViewModel.EmptyCompletedTask, task);
        }
    }

    [ExcludeFromCodeCoverage]
    class SimulatedLongRunningTask
    {
        public SimulatedLongRunningTask(CancellationTokenSource cancel)
        {
            Cancel = cancel;
        }

        private CancellationTokenSource Cancel { get; set; }

        public bool DelayExpired { get; private set; }

        public void Simulate()
        {
            Task.Delay(1000, Cancel.Token).GetAwaiter().GetResult();

            DelayExpired = true;
        }

        public string SimulateResult()
        {
            Simulate();

            return "test";
        }
    }
}
