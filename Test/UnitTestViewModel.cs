﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace Acrotech.PortableViewModel.Test
{
    public class UnitTestViewModel : BaseViewModel
    {
        public UnitTestViewModel()
        {
            Reset();
        }

        public bool OnTextChangedOccurred { get; private set; }
        public string PreviousTextValue { get; private set; }

        #region Text

        private string propText = default(string);

        public const string TextPropertyName = "Text";

        public string Text
        {
            get { return propText; }
            set { RaiseAndSetIfChanged(ref propText, value, OnTextChanged); }
        }

        protected virtual void OnTextChanged(string previousValue)
        {
            OnTextChangedOccurred = true;
            PreviousTextValue = previousValue;
        }

        #endregion

        #region Reset Methods

        public void Reset()
        {
            ResetOnTextChanged();
            PreviousTextValue = null;
        }

        public void ResetOnTextChanged()
        {
            OnTextChangedOccurred = false;
        }

        #endregion

        public void TestOnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            base.OnPropertyChanged(propertyName);
        }

        #region TestRaiseAndSetIfChanged Methods

        public T TestRaiseAndSetIfChanged<T>(ref T backingField, T newValue, Action<T> onChanged = null, Action<T> onChanging = null, [CallerMemberName] string propertyName = null)
        {
            return RaiseAndSetIfChanged(ref backingField, newValue, onChanged, onChanging, propertyName);
        }

        public T TestRaiseAndSetIfChanged<T>(ref T backingField, T newValue, string propertyName, Action<T> onChanged = null, Action<T> onChanging = null)
        {
            return RaiseAndSetIfChanged(ref backingField, newValue, propertyName, onChanged, onChanging);
        }

        public T TestRaiseAndSetIfChanged<T>(ref T backingField, T newValue, PropertyChangedEventArgs args, Action<T> onChanged = null, Action<T> onChanging = null)
        {
            return RaiseAndSetIfChanged(ref backingField, newValue, args, onChanged, onChanging);
        }

        public T TestRaiseAndSetIfChanged<T>(ref T backingField, T newValue, Func<PropertyChangedEventArgs> argsCreator, Action<T> onChanged = null, Action<T> onChanging = null)
        {
            return RaiseAndSetIfChanged(ref backingField, newValue, argsCreator, onChanged, onChanging);
        }

        #endregion
    }
}
