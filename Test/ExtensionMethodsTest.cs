﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.ComponentModel;

namespace Acrotech.PortableViewModel.Test
{
    [TestClass]
    public class ExtensionMethodsTest
    {
        [TestMethod]
        public void RaiseAndSetIfChangedWithCallerMemberNameTest()
        {
            PropertyChangedEventArgs args = null;
            var handler = new PropertyChangedEventHandler((s, e) => { args = e; });
            string backingField = null;

            var source = new UnitTestViewModel();

            Assert.AreEqual("test", source.RaiseAndSetIfChanged(handler, ref backingField, "test"));
            Assert.AreEqual("test", backingField);
            Assert.IsNotNull(args);
            Assert.AreEqual("RaiseAndSetIfChangedWithCallerMemberNameTest", args.PropertyName);
        }

        [TestMethod]
        public void RaiseAndSetIfChangedWithStringTest()
        {
            PropertyChangedEventArgs args = null;
            var handler = new PropertyChangedEventHandler((s, e) => { args = e; });
            string backingField = null;

            var source = new UnitTestViewModel();

            Assert.AreEqual("test", source.RaiseAndSetIfChanged(handler, ref backingField, "test", "Field", null, null));
            Assert.AreEqual("test", backingField);
            Assert.IsNotNull(args);
            Assert.AreEqual("Field", args.PropertyName);
        }

        [TestMethod]
        public void RaiseAndSetIfChangedWithEventArgsTest()
        {
            PropertyChangedEventArgs args = null;
            var handler = new PropertyChangedEventHandler((s, e) => { args = e; });
            string backingField = null;
            var testArgs = new PropertyChangedEventArgs(null);

            var source = new UnitTestViewModel();

            Assert.AreEqual("test", source.RaiseAndSetIfChanged(handler, ref backingField, "test", testArgs, null, null));
            Assert.AreEqual("test", backingField);
            Assert.AreEqual(testArgs, args);
        }

        [TestMethod]
        public void RaiseAndSetIfChangedWithEventArgsCreatorTest()
        {
            INotifyPropertyChanged source = null;
            PropertyChangedEventHandler handler = null;
            string backingField = null;
            Func<PropertyChangedEventArgs> argsCreator = null;
            Action<string> onChanged = null;
            Action<string> onChanging = null;
            Action<PropertyChangedEventHandler, INotifyPropertyChanged, Func<PropertyChangedEventArgs>> notifyAction = null;

            // null source
            backingField = null;
            Assert.AreEqual("test", source.RaiseAndSetIfChanged(handler, ref backingField, "test", argsCreator, onChanged, onChanging, notifyAction));
            Assert.IsNull(backingField);

            // source
            source = new UnitTestViewModel();
            backingField = null;
            Assert.AreEqual("test", source.RaiseAndSetIfChanged(handler, ref backingField, "test", argsCreator, onChanged, onChanging, notifyAction));
            Assert.AreEqual("test", backingField);

            // onChanged delegate
            string prevValue = null;
            onChanged = x => prevValue = x;
            backingField = null;
            Assert.AreEqual("test1", source.RaiseAndSetIfChanged(handler, ref backingField, "test1", argsCreator, onChanged, onChanging, notifyAction));
            Assert.AreEqual("test1", backingField);
            Assert.IsNull(prevValue);
            Assert.AreEqual("test2", source.RaiseAndSetIfChanged(handler, ref backingField, "test2", argsCreator, onChanged, onChanging, notifyAction));
            Assert.AreEqual("test2", backingField);
            Assert.AreEqual("test1", prevValue);
            onChanged = null;

            // onChanging delegate
            string newValue = null;
            onChanging = x => newValue = x;
            backingField = null;
            Assert.AreEqual("test", source.RaiseAndSetIfChanged(handler, ref backingField, "test", argsCreator, onChanged, onChanging, notifyAction));
            Assert.AreEqual("test", backingField);
            Assert.AreEqual("test", newValue);
            onChanging = null;

            // handler
            object sender = null;
            PropertyChangedEventArgs args = null;
            handler = new PropertyChangedEventHandler((s, e) => { sender = s; args = e; });
            backingField = null;
            Assert.AreEqual("test", source.RaiseAndSetIfChanged(handler, ref backingField, "test", argsCreator, onChanged, onChanging, notifyAction));
            Assert.AreEqual("test", backingField);
            Assert.AreEqual(source, sender);
            Assert.IsNull(args);

            // argsCreator
            var testArgs = new PropertyChangedEventArgs(null);
            argsCreator = () => testArgs;
            sender = null;
            args = null;
            backingField = null;
            Assert.AreEqual("test", source.RaiseAndSetIfChanged(handler, ref backingField, "test", argsCreator, onChanged, onChanging, notifyAction));
            Assert.AreEqual("test", backingField);
            Assert.AreEqual(testArgs, args);

            // notifyAction
            PropertyChangedEventHandler notifyHandler = null;
            INotifyPropertyChanged notifySource = null;
            Func<PropertyChangedEventArgs> notifyArgsCreator = null;
            notifyAction = (h, s, a) => { notifyHandler = h; notifySource = s; notifyArgsCreator = a; };
            backingField = null;
            Assert.AreEqual("test", source.RaiseAndSetIfChanged(handler, ref backingField, "test", argsCreator, onChanged, onChanging, notifyAction));
            Assert.AreEqual("test", backingField);
            Assert.AreEqual(handler, notifyHandler);
            Assert.AreEqual(source, notifySource);
            Assert.AreEqual(argsCreator, notifyArgsCreator);
        }

        [TestMethod]
        public void PerformPropertyChangeNotificationWithStringTest()
        {
            string propertyName = null;
            var handler = new PropertyChangedEventHandler((s, e) => { propertyName = e.PropertyName; });
            handler.PerformPropertyChangeNotification(null, "test");
            Assert.AreEqual("test", propertyName);
        }

        [TestMethod]
        public void PerformPropertyChangeNotificationWithArgsTest()
        {
            PropertyChangedEventArgs args = null;
            var testArgs = new PropertyChangedEventArgs("test");
            var handler = new PropertyChangedEventHandler((s, e) => { args = e; });
            handler.PerformPropertyChangeNotification(null, testArgs);
            Assert.AreEqual(testArgs, args);
        }

        [TestMethod]
        public void PerformPropertyChangeNotificationWithArgsCreatorTest()
        {
            object handlerSource = null;
            PropertyChangedEventArgs args = null;
            PropertyChangedEventHandler handler = null;
            INotifyPropertyChanged source = new UnitTestViewModel();
            Func<PropertyChangedEventArgs> argsCreator = null;
            var argsCreatorCalled = false;
            var testArgs = new PropertyChangedEventArgs("Field");

            argsCreator = () => { argsCreatorCalled = true; return testArgs; };
            handler.PerformPropertyChangeNotification(source, argsCreator);
            Assert.IsFalse(argsCreatorCalled);

            handler = new PropertyChangedEventHandler((s, e) => { handlerSource = s; args = e; });
            argsCreatorCalled = false;
            handler.PerformPropertyChangeNotification(source, argsCreator);
            Assert.IsTrue(argsCreatorCalled);
            Assert.AreEqual(source, handlerSource);
            Assert.AreEqual(testArgs, args);

            argsCreator = null;
            handlerSource = null;
            args = null;
            handler.PerformPropertyChangeNotification(source, argsCreator);
            Assert.AreEqual(source, handlerSource);
            Assert.IsNull(args);
        }
    }
}
