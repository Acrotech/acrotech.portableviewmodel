﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Threading.Tasks;
using System.ComponentModel;
using System.Threading;

namespace Acrotech.PortableViewModel.Test
{
    [TestClass]
    public class UnitTestTaskViewModelTest
    {
        [TestMethod]
        public void ConstructorTest()
        {
            var scheduler = TaskScheduler.Default;

            var vm = new UnitTestTaskViewModel(scheduler);
            Assert.AreEqual(scheduler, vm.UIScheduler);
        }

        [TestMethod]
        public void TextChangeTest()
        {
            SynchronizationContext.SetSynchronizationContext(new SynchronizationContext());
            var uiScheduler = TaskScheduler.FromCurrentSynchronizationContext();
            TaskScheduler scheduler = null;
            ManualResetEventSlim wait = new ManualResetEventSlim();
            var handler = new PropertyChangedEventHandler((s, e) => { scheduler = TaskScheduler.Current; wait.Set(); });

            var vm = new UnitTestTaskViewModel(uiScheduler);

            vm.PropertyChanged += handler;

            vm.Text = "test";
            wait.Wait();
            Assert.AreEqual(uiScheduler, scheduler);
            Assert.AreEqual("test", vm.Text);

            vm.PropertyChanged -= handler;
        }
    }
}
