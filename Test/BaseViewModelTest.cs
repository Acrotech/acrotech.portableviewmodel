﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Diagnostics.CodeAnalysis;
using System.ComponentModel;

namespace Acrotech.PortableViewModel.Test
{
    [TestClass]
    public class BaseViewModelTest
    {
        #region OnPropertyChanged Tests

        [TestMethod]
        public void OnPropertyChangedWithStringTest()
        {
            var vm = new UnitTestViewModel();
            var po = new PrivateObject(vm);

            var handlerCalled = false;

            var handler = new PropertyChangedEventHandler((s, e) => handlerCalled = true);

            vm.PropertyChanged += handler;

            po.Invoke("OnPropertyChanged", "test");

            Assert.IsTrue(handlerCalled);

            vm.PropertyChanged -= handler;
        }

        [TestMethod]
        public void OnPropertyChangedWithEventArgsTest()
        {
            var vm = new UnitTestViewModel();
            var po = new PrivateObject(vm);

            var handlerCalled = false;

            var handler = new PropertyChangedEventHandler((s, e) => handlerCalled = true);

            vm.PropertyChanged += handler;

            po.Invoke("OnPropertyChanged", new PropertyChangedEventArgs("test"));

            Assert.IsTrue(handlerCalled);

            vm.PropertyChanged -= handler;
        }

        [TestMethod]
        public void OnPropertyChangedWithEventArgsCreatorTest()
        {
            var vm = new UnitTestViewModel();
            var po = new PrivateObject(vm);

            var handlerCalled = false;

            var handler = new PropertyChangedEventHandler((s, e) => handlerCalled = true);

            vm.PropertyChanged += handler;

            po.Invoke("OnPropertyChanged", new Func<PropertyChangedEventArgs>(() => new PropertyChangedEventArgs("test")));

            Assert.IsTrue(handlerCalled);

            vm.PropertyChanged -= handler;
        }

        [TestMethod]
        public void OnPropertyChangedWithHandlerAndEventArgsTest()
        {
            var vm = new UnitTestViewModel();
            var po = new PrivateObject(vm);
            var paramTypes = new[] { typeof(PropertyChangedEventHandler), typeof(Func<PropertyChangedEventArgs>) };

            var handlerCalled = false;
            var argsCreated = false;
            Func<PropertyChangedEventArgs> argsCreator = () => { argsCreated = true; return new PropertyChangedEventArgs("test"); };
            PropertyChangedEventHandler handler = null;

            po.Invoke("OnPropertyChanged", paramTypes, new object[] { handler, argsCreator });
            Assert.IsFalse(handlerCalled);
            Assert.IsFalse(argsCreated);

            handler = new PropertyChangedEventHandler((s, e) => handlerCalled = true);
            po.Invoke("OnPropertyChanged", paramTypes, new object[] { handler, argsCreator });
            Assert.IsTrue(handlerCalled);
            Assert.IsTrue(argsCreated);

            argsCreated = false;
            handlerCalled = false;
            PropertyChangedEventArgs args = null;
            handler = new PropertyChangedEventHandler((s, e) => { handlerCalled = true; args = e; });
            po.Invoke("OnPropertyChanged", paramTypes, new object[] { handler, null });
            Assert.IsTrue(handlerCalled);
            Assert.IsFalse(argsCreated);
            Assert.IsNull(args);
        }

        #endregion

        #region RaiseAndSetIfChanged Tests

        [TestMethod]
        public void RaiseAndSetIfChangedWithCallerMemberNameTest()
        {
            var vm = new UnitTestViewModel();

            var handlerCalled = false;
            string backingField = null;
            string propertyName = null;

            var handler = new PropertyChangedEventHandler((s, e) => { handlerCalled = true; propertyName = e.PropertyName; });

            vm.PropertyChanged += handler;

            var result = vm.TestRaiseAndSetIfChanged(ref backingField, "test");

            Assert.AreEqual("test", result);
            Assert.AreEqual("test", backingField);
            Assert.IsTrue(handlerCalled);
            Assert.AreEqual("RaiseAndSetIfChangedWithCallerMemberNameTest", propertyName);

            vm.PropertyChanged -= handler;
        }

        [TestMethod]
        public void RaiseAndSetIfChangedWithStringTest()
        {
            var vm = new UnitTestViewModel();

            var handlerCalled = false;
            string backingField = null;
            string propertyName = null;

            var handler = new PropertyChangedEventHandler((s, e) => { handlerCalled = true; propertyName = e.PropertyName; });

            vm.PropertyChanged += handler;

            var result = vm.TestRaiseAndSetIfChanged(ref backingField, "test", "Field");

            Assert.AreEqual("test", result);
            Assert.AreEqual("test", backingField);
            Assert.IsTrue(handlerCalled);
            Assert.AreEqual("Field", propertyName);

            vm.PropertyChanged -= handler;
        }

        [TestMethod]
        public void RaiseAndSetIfChangedWithEventArgsTest()
        {
            var vm = new UnitTestViewModel();

            var handlerCalled = false;
            string backingField = null;
            PropertyChangedEventArgs args = null;
            PropertyChangedEventArgs testArgs = null;

            var handler = new PropertyChangedEventHandler((s, e) => { handlerCalled = true; args = e; });

            vm.PropertyChanged += handler;

            testArgs = new PropertyChangedEventArgs("Field");
            var result = vm.TestRaiseAndSetIfChanged(ref backingField, "test", testArgs);

            Assert.AreEqual("test", result);
            Assert.AreEqual("test", backingField);
            Assert.IsTrue(handlerCalled);
            Assert.AreEqual(testArgs, args);

            vm.PropertyChanged -= handler;
        }

        [TestMethod]
        public void RaiseAndSetIfChangedWithEventArgsCreatorTest()
        {
            var vm = new UnitTestViewModel();

            var handlerCalled = false;
            string backingField = null;
            PropertyChangedEventArgs args = null;
            PropertyChangedEventArgs testArgs = null;

            var handler = new PropertyChangedEventHandler((s, e) => { handlerCalled = true; args = e; });

            vm.PropertyChanged += handler;

            testArgs = new PropertyChangedEventArgs("Field");
            var result = vm.TestRaiseAndSetIfChanged(ref backingField, "test", () => testArgs);

            Assert.AreEqual("test", result);
            Assert.AreEqual("test", backingField);
            Assert.IsTrue(handlerCalled);
            Assert.AreEqual(testArgs, args);

            vm.PropertyChanged -= handler;
        }

        #endregion
    }
}
