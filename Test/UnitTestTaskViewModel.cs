﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Acrotech.PortableViewModel.Test
{
    public class UnitTestTaskViewModel : TaskBaseViewModel
    {
        public UnitTestTaskViewModel()
            : base()
        {
        }

        public UnitTestTaskViewModel(TaskScheduler uiScheduler)
            : base(uiScheduler)
        {
        }

        #region Text

        private string propText = default(string);

        public const string TextPropertyName = "Text";

        public string Text
        {
            get { return propText; }
            set { RaiseAndSetIfChanged(ref propText, value); }
        }

        #endregion

        public Task TestRunOnUiThread(Action action, int delay = 0, CancellationToken? cancellationToken = null)
        {
            return RunOnUiThread(action, delay, cancellationToken);
        }

        public Task<T> TestRunOnUiThread<T>(Func<T> action, int delay = 0, CancellationToken? cancellationToken = null)
        {
            return RunOnUiThread(action, delay, cancellationToken);
        }

        public Task TestRunOffUiThread(Action action, int delay = 0, CancellationToken? cancellationToken = null)
        {
            return RunOffUiThread(action, delay, cancellationToken);
        }

        public Task<T> TestRunOffUiThread<T>(Func<T> action, int delay = 0, CancellationToken? cancellationToken = null)
        {
            return RunOffUiThread(action, delay, cancellationToken);
        }
    }
}
