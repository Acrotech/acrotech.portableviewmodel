﻿using Acrotech.PortableViewModel.Commands;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Acrotech.PortableViewModel.Test.Commands
{
    [TestClass]
    public class ExtensionMethodsTest
    {
        [TestMethod]
        public void CanExecuteTest()
        {
            var cmd = new UnitTestCommand();

            cmd.Reset();
            var result = cmd.CanExecute();
            Assert.IsNull(cmd.LastParameter);
            Assert.IsTrue(cmd.CanExecuteCalled);
            Assert.AreEqual(cmd.CanExecuteResult, result);

            cmd.Reset();
            cmd.CanExecuteResult = true;
            result = cmd.CanExecute();
            Assert.IsNull(cmd.LastParameter);
            Assert.IsTrue(cmd.CanExecuteCalled);
            Assert.AreEqual(cmd.CanExecuteResult, result);
        }

        [TestMethod]
        public void ExecuteTest()
        {
            var cmd = new UnitTestCommand();

            cmd.Reset();
            cmd.Execute();
            Assert.IsNull(cmd.LastParameter);
        }
    }
}
