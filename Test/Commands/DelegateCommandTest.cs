﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Acrotech.PortableViewModel.Commands;
using System.Diagnostics.CodeAnalysis;
using System.Threading;
using System.Threading.Tasks;

namespace Acrotech.PortableViewModel.Test.Commands
{
    [TestClass]
    public class DelegateCommandTest
    {
        [TestMethod]
        public void SchedulerConstructorTest()
        {
            var cmd = new DelegateCommand((Action)null);
            Assert.IsNull(cmd.UIScheduler);

            SynchronizationContext.SetSynchronizationContext(new SynchronizationContext());
            var uiScheduler = TaskScheduler.FromCurrentSynchronizationContext();

            cmd = new DelegateCommand((Action)null);
            Assert.IsNull(cmd.UIScheduler);

            cmd = new DelegateCommand((Action<object>)null);
            Assert.IsNull(cmd.UIScheduler);

            DelegateCommand.DefaultUITaskScheduler = uiScheduler;
            cmd = new DelegateCommand((Action)null);
            Assert.AreEqual(uiScheduler, cmd.UIScheduler);

            cmd = new DelegateCommand((Action<object>)null);
            Assert.AreEqual(uiScheduler, cmd.UIScheduler);

            var scheduler = TaskScheduler.FromCurrentSynchronizationContext();
            cmd = new DelegateCommand((Action)null, uiScheduler: scheduler);
            Assert.AreEqual(scheduler, cmd.UIScheduler);

            cmd = new DelegateCommand((Action<object>)null, uiScheduler: scheduler);
            Assert.AreEqual(scheduler, cmd.UIScheduler);
        }

        [TestMethod]
        public void NoParamActionConstructorTest()
        {
            var canExecuteCalled = false;
            var executeCalled = false;

            Func<bool> canExecute = () => { canExecuteCalled = true; return false; };
            Action execute = () => { executeCalled = true; };

            var cmd = new DelegateCommand((Action)null);
            Assert.IsNull(cmd.CanExecuteAction);
            Assert.IsNull(cmd.ExecuteAction);

            cmd = new DelegateCommand(execute);
            Assert.IsNull(cmd.CanExecuteAction);
            Assert.IsNotNull(cmd.ExecuteAction);

            cmd = new DelegateCommand((Action)null, (Func<bool>)null);
            Assert.IsNull(cmd.CanExecuteAction);
            Assert.IsNull(cmd.ExecuteAction);

            cmd = new DelegateCommand(execute, (Func<bool>)null);
            Assert.IsNull(cmd.CanExecuteAction);
            Assert.IsNotNull(cmd.ExecuteAction);

            cmd = new DelegateCommand((Action)null, canExecute);
            Assert.IsNotNull(cmd.CanExecuteAction);
            Assert.IsNull(cmd.ExecuteAction);

            cmd = new DelegateCommand(execute, canExecute);
            Assert.IsNotNull(cmd.CanExecuteAction);
            Assert.IsNotNull(cmd.ExecuteAction);

            canExecuteCalled = false;
            cmd.CanExecuteAction(null);
            Assert.IsTrue(canExecuteCalled);

            executeCalled = false;
            cmd.ExecuteAction(null);
            Assert.IsTrue(executeCalled);
        }

        [TestMethod]
        public void SingleParamActionConstructorTest()
        {
            var canExecuteCalled = false;
            var executeCalled = false;

            Func<object, bool> canExecute = x => { canExecuteCalled = true; return false; };
            Action<object> execute = x => { executeCalled = true; };

            var cmd = new DelegateCommand((Action<object>)null);
            Assert.IsNull(cmd.CanExecuteAction);
            Assert.IsNull(cmd.ExecuteAction);

            cmd = new DelegateCommand(execute);
            Assert.IsNull(cmd.CanExecuteAction);
            Assert.AreEqual(execute, cmd.ExecuteAction);

            cmd = new DelegateCommand((Action<object>)null, (Func<object, bool>)null);
            Assert.IsNull(cmd.CanExecuteAction);
            Assert.IsNull(cmd.ExecuteAction);

            cmd = new DelegateCommand(execute, (Func<object, bool>)null);
            Assert.IsNull(cmd.CanExecuteAction);
            Assert.AreEqual(execute, cmd.ExecuteAction);

            cmd = new DelegateCommand((Action<object>)null, canExecute);
            Assert.AreEqual(canExecute, cmd.CanExecuteAction);
            Assert.IsNull(cmd.ExecuteAction);

            cmd = new DelegateCommand(execute, canExecute);
            Assert.AreEqual(canExecute, cmd.CanExecuteAction);
            Assert.AreEqual(execute, cmd.ExecuteAction);

            canExecuteCalled = false;
            cmd.CanExecuteAction(null);
            Assert.IsTrue(canExecuteCalled);

            executeCalled = false;
            cmd.ExecuteAction(null);
            Assert.IsTrue(executeCalled);
        }

        [TestMethod]
        public void GenericSingleParamActionConstructorTest()
        {
            var canExecuteCalled = false;
            var executeCalled = false;

            Func<string, bool> canExecute = x => { canExecuteCalled = true; return false; };
            Action<string> execute = x => { executeCalled = true; };

            var cmd = new DelegateCommand<string>((Action<string>)null);
            Assert.IsNull(cmd.CanExecuteAction);
            Assert.IsNull(cmd.ExecuteAction);

            cmd = new DelegateCommand<string>(execute);
            Assert.IsNull(cmd.CanExecuteAction);
            Assert.IsNotNull(cmd.ExecuteAction);

            cmd = new DelegateCommand<string>((Action<string>)null, (Func<string, bool>)null);
            Assert.IsNull(cmd.CanExecuteAction);
            Assert.IsNull(cmd.ExecuteAction);

            cmd = new DelegateCommand<string>(execute, (Func<string, bool>)null);
            Assert.IsNull(cmd.CanExecuteAction);
            Assert.IsNotNull(cmd.ExecuteAction);

            cmd = new DelegateCommand<string>((Action<string>)null, canExecute);
            Assert.IsNotNull(cmd.CanExecuteAction);
            Assert.IsNull(cmd.ExecuteAction);

            cmd = new DelegateCommand<string>(execute, canExecute);
            Assert.IsNotNull(cmd.CanExecuteAction);
            Assert.IsNotNull(cmd.ExecuteAction);

            canExecuteCalled = false;
            cmd.CanExecuteAction(null);
            Assert.IsTrue(canExecuteCalled);

            executeCalled = false;
            cmd.ExecuteAction(null);
            Assert.IsTrue(executeCalled);
        }

        [TestMethod]
        public void RaiseCanExecuteChangedTest()
        {
            DelegateCommand.DefaultUITaskScheduler = null;

            var cmd = new DelegateCommand((Action)null);
            var po = new PrivateObject(cmd);

            object sender = null;
            EventArgs args = null;

            var handler = new EventHandler((s, e) => { sender = s; args = e; });

            cmd.CanExecuteChanged += handler;

            sender = null;
            args = null;
            cmd.RaiseCanExecuteChanged();
            Assert.AreEqual(cmd, sender);
            Assert.AreEqual(EventArgs.Empty, args);

            sender = null;
            args = null;
            po.Invoke("RaiseCanExecuteChanged", handler);
            Assert.AreEqual(cmd, sender);
            Assert.AreEqual(EventArgs.Empty, args);

            sender = null;
            args = null;
            po.Invoke("RaiseCanExecuteChanged", (EventHandler)null);
            Assert.IsNull(sender);
            Assert.IsNull(args);

            cmd.CanExecuteChanged -= handler;
        }

        [TestMethod]
        public void RaiseCanExecuteChangedWithSchedulerTest()
        {
            SynchronizationContext.SetSynchronizationContext(new SynchronizationContext());
            var uiScheduler = TaskScheduler.FromCurrentSynchronizationContext();
            
            TaskScheduler scheduler = null;
            var wait = new ManualResetEventSlim();
            var handler = new EventHandler((s, e) => { scheduler = TaskScheduler.Current; wait.Set(); });

            var cmd = new DelegateCommand((Action)null, uiScheduler: uiScheduler);

            cmd.CanExecuteChanged += handler;

            cmd.RaiseCanExecuteChanged();
            wait.Wait();
            Assert.AreEqual(uiScheduler, scheduler);

            cmd.CanExecuteChanged -= handler;
        }

        [TestMethod]
        public void RaiseCanExecuteChangedOnUiTest()
        {
            DelegateCommand.DefaultUITaskScheduler = null;
            SynchronizationContext.SetSynchronizationContext(new SynchronizationContext());
            var uiScheduler = TaskScheduler.FromCurrentSynchronizationContext();

            var cmd = new DelegateCommand((Action)null);
            var po = new PrivateObject(cmd);

            TaskScheduler scheduler = null;
            var handler = new EventHandler((s, e) => { scheduler = TaskScheduler.Current; });

            var task = (Task)po.Invoke("RaiseCanExecuteChangedOnUi", (EventHandler)null);
            Assert.IsNotNull(task);
            Assert.IsTrue(task.IsCompleted);

            cmd.CanExecuteChanged += handler;

            task = (Task)po.Invoke("RaiseCanExecuteChangedOnUi", handler);
            Assert.IsNotNull(task);
            task.Wait();
            Assert.AreEqual(TaskScheduler.Default, scheduler);

            cmd = new DelegateCommand((Action)null, uiScheduler: uiScheduler);
            po = new PrivateObject(cmd);
            scheduler = null;
            task = (Task)po.Invoke("RaiseCanExecuteChangedOnUi", handler);
            Assert.IsNotNull(task);
            task.Wait();
            Assert.AreEqual(uiScheduler, scheduler);

            cmd.CanExecuteChanged -= handler;
        }

        [TestMethod]
        public void CanExecuteTest()
        {
            var canExecuteCalled = false;
            object canExecuteParam = null;
            var canExecuteResult = false;

            Func<object, bool> canExecute = x => { canExecuteCalled = true; canExecuteParam = x; return canExecuteResult; };

            var cmd = new DelegateCommand((Action<object>)null, (Func<object, bool>)null);
            Assert.AreEqual(true, cmd.CanExecute(null));

            canExecuteCalled = false;
            canExecuteParam = null;
            canExecuteResult = false;
            cmd = new DelegateCommand((Action<object>)null, canExecute);
            var result = cmd.CanExecute(null);
            Assert.IsTrue(canExecuteCalled);
            Assert.IsNull(canExecuteParam);
            Assert.AreEqual(canExecuteResult, result);

            canExecuteCalled = false;
            canExecuteParam = null;
            canExecuteResult = false;
            result = cmd.CanExecute("test");
            Assert.IsTrue(canExecuteCalled);
            Assert.AreEqual("test", canExecuteParam);
            Assert.AreEqual(canExecuteResult, result);

            canExecuteCalled = false;
            canExecuteParam = null;
            canExecuteResult = true;
            result = cmd.CanExecute(null);
            Assert.IsTrue(canExecuteCalled);
            Assert.IsNull(canExecuteParam);
            Assert.AreEqual(canExecuteResult, result);
        }

        [TestMethod]
        public void ExecuteTest()
        {
            var executeCalled = false;
            object executeParam = null;

            Action<object> execute = x => { executeCalled = true; executeParam = x; };

            var cmd = new DelegateCommand(execute);

            executeCalled = false;
            executeParam = null;
            cmd.Execute(null);
            Assert.IsTrue(executeCalled);
            Assert.IsNull(executeParam);

            executeCalled = false;
            executeParam = null;
            cmd.Execute("test");
            Assert.IsTrue(executeCalled);
            Assert.AreEqual("test", executeParam);

            cmd = new DelegateCommand((Action<object>)null);

            // ensure no exceptions occur
            cmd.Execute(null);
        }

        [TestMethod]
        public void GenericCanExecuteTest()
        {
            var canExecuteCalled = false;
            string canExecuteParam = null;
            var canExecuteResult = false;

            Func<string, bool> canExecute = x => { canExecuteCalled = true; canExecuteParam = x; return canExecuteResult; };

            var cmd = new DelegateCommand<string>((Action<object>)null, (Func<object, bool>)null);
            Assert.AreEqual(true, cmd.CanExecute(null));

            cmd = new DelegateCommand<string>((Action<string>)null, canExecute);

            canExecuteCalled = false;
            canExecuteParam = null;
            canExecuteResult = false;
            var result = cmd.CanExecute(null);
            Assert.IsTrue(canExecuteCalled);
            Assert.IsNull(canExecuteParam);
            Assert.AreEqual(canExecuteResult, result);

            canExecuteCalled = false;
            canExecuteParam = null;
            canExecuteResult = false;
            result = ((ICommand)cmd).CanExecute(null);
            Assert.IsTrue(canExecuteCalled);
            Assert.IsNull(canExecuteParam);
            Assert.AreEqual(canExecuteResult, result);

            canExecuteCalled = false;
            canExecuteParam = null;
            canExecuteResult = false;
            result = cmd.CanExecute("test");
            Assert.IsTrue(canExecuteCalled);
            Assert.AreEqual("test", canExecuteParam);
            Assert.AreEqual(canExecuteResult, result);

            canExecuteCalled = false;
            canExecuteParam = null;
            canExecuteResult = false;
            result = ((ICommand)cmd).CanExecute("test");
            Assert.IsTrue(canExecuteCalled);
            Assert.AreEqual("test", canExecuteParam);
            Assert.AreEqual(canExecuteResult, result);

            canExecuteCalled = false;
            canExecuteParam = null;
            canExecuteResult = true;
            result = cmd.CanExecute(null);
            Assert.IsTrue(canExecuteCalled);
            Assert.IsNull(canExecuteParam);
            Assert.AreEqual(canExecuteResult, result);

            canExecuteCalled = false;
            canExecuteParam = null;
            canExecuteResult = true;
            result = ((ICommand)cmd).CanExecute(null);
            Assert.IsTrue(canExecuteCalled);
            Assert.IsNull(canExecuteParam);
            Assert.AreEqual(canExecuteResult, result);
        }

        [TestMethod]
        [ExpectedException(typeof(InvalidCastException))]
        [ExcludeFromCodeCoverage]
        public void GenericTypeMismatchCanExecuteTest()
        {
            var cmd = new DelegateCommand<string>((Action<string>)null, (Func<string, bool>)null);
            cmd.CanExecute(0);
        }

        [TestMethod]
        [ExpectedException(typeof(InvalidCastException))]
        [ExcludeFromCodeCoverage]
        public void GenericTypeMismatchICommandCanExecuteTest()
        {
            ICommand cmd = new DelegateCommand<string>((Action<string>)null, (Func<string, bool>)null);
            cmd.CanExecute(0);
        }

        [TestMethod]
        public void GenericExecuteTest()
        {
            var executeCalled = false;
            string executeParam = null;

            Action<string> execute = x => { executeCalled = true; executeParam = x; };

            var cmd = new DelegateCommand<string>(execute);

            executeCalled = false;
            executeParam = null;
            cmd.Execute(null);
            Assert.IsTrue(executeCalled);
            Assert.IsNull(executeParam);

            executeCalled = false;
            executeParam = null;
            ((ICommand)cmd).Execute(null);
            Assert.IsTrue(executeCalled);
            Assert.IsNull(executeParam);

            executeCalled = false;
            executeParam = null;
            cmd.Execute("test");
            Assert.IsTrue(executeCalled);
            Assert.AreEqual("test", executeParam);

            executeCalled = false;
            executeParam = null;
            ((ICommand)cmd).Execute("test");
            Assert.IsTrue(executeCalled);
            Assert.AreEqual("test", executeParam);

            cmd = new DelegateCommand<string>((Action<string>)null);

            // ensure no exceptions occur
            cmd.Execute(null);
        }

        [TestMethod]
        [ExpectedException(typeof(InvalidCastException))]
        [ExcludeFromCodeCoverage]
        public void GenericTypeMismatchExecuteTest()
        {
            var cmd = new DelegateCommand<string>((Action<string>)null);
            cmd.Execute(0);
        }

        [TestMethod]
        [ExpectedException(typeof(InvalidCastException))]
        [ExcludeFromCodeCoverage]
        public void GenericTypeMismatchICommandExecuteTest()
        {
            ICommand cmd = new DelegateCommand<string>((Action<string>)null);
            cmd.Execute(0);
        }
    }
}
