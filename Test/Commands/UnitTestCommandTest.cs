﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Acrotech.PortableViewModel.Test.Commands
{
    [TestClass]
    public class UnitTestCommandTest
    {
        [TestMethod]
        public void ConstructorTest()
        {
            var cmd = new UnitTestCommand();

            Assert.IsFalse(cmd.CanExecuteResult);
            Assert.IsNull(cmd.LastParameter);
            Assert.IsFalse(cmd.CanExecuteCalled);
            Assert.IsFalse(cmd.ExecuteCalled);
        }

        [TestMethod]
        public void ResetTest()
        {
            var cmd = new UnitTestCommand();
            var po = new PrivateObject(cmd);

            cmd.CanExecuteResult = true;
            po.SetProperty("LastParameter", "test");
            po.SetProperty("CanExecuteCalled", true);
            po.SetProperty("ExecuteCalled", true);

            cmd.Reset();

            Assert.IsFalse(cmd.CanExecuteResult);
            Assert.IsNull(cmd.LastParameter);
            Assert.IsFalse(cmd.CanExecuteCalled);
            Assert.IsFalse(cmd.ExecuteCalled);
        }

        [TestMethod]
        public void RaiseCanExecuteChangedTest()
        {
            var cmd = new UnitTestCommand();

            object sender = null;
            EventArgs args = null;

            var handler = new EventHandler((s, e) => { sender = s; args = e; });

            cmd.CanExecuteChanged += handler;

            sender = null;
            args = null;
            cmd.RaiseCanExecuteChanged();
            Assert.AreEqual(cmd, sender);
            Assert.AreEqual(EventArgs.Empty, args);

            cmd.CanExecuteChanged -= handler;

            sender = null;
            args = null;
            cmd.RaiseCanExecuteChanged();
            Assert.IsNull(sender);
            Assert.IsNull(args);
        }

        [TestMethod]
        public void CanExecuteTest()
        {
            var cmd = new UnitTestCommand();

            cmd.Reset();
            var result = cmd.CanExecute(null);
            Assert.IsNull(cmd.LastParameter);
            Assert.IsTrue(cmd.CanExecuteCalled);
            Assert.AreEqual(cmd.CanExecuteResult, result);

            cmd.Reset();
            result = cmd.CanExecute("test");
            Assert.AreEqual("test", cmd.LastParameter);
            Assert.IsTrue(cmd.CanExecuteCalled);
            Assert.AreEqual(cmd.CanExecuteResult, result);

            cmd.Reset();
            cmd.CanExecuteResult = true;
            result = cmd.CanExecute(null);
            Assert.IsNull(cmd.LastParameter);
            Assert.IsTrue(cmd.CanExecuteCalled);
            Assert.AreEqual(cmd.CanExecuteResult, result);
        }

        [TestMethod]
        public void ExecuteTest()
        {
            var cmd = new UnitTestCommand();

            cmd.Reset();
            cmd.Execute(null);
            Assert.IsNull(cmd.LastParameter);

            cmd.Reset();
            cmd.Execute("test");
            Assert.AreEqual("test", cmd.LastParameter);
        }
    }
}
