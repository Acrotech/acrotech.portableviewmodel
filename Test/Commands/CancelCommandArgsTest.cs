﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Acrotech.PortableViewModel.Commands;

namespace Acrotech.PortableViewModel.Test.Commands
{
    [TestClass]
    public class CancelCommandArgsTest
    {
        [TestMethod]
        public void ConstructorTest()
        {
            var args = new CancelCommandArgs();
            Assert.AreEqual(false, args.IsCancelRequested);

            args = new CancelCommandArgs(false);
            Assert.AreEqual(false, args.IsCancelRequested);

            args = new CancelCommandArgs(true);
            Assert.AreEqual(true, args.IsCancelRequested);
        }
    }
}
