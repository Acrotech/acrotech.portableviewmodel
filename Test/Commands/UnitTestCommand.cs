﻿using Acrotech.PortableViewModel.Commands;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Acrotech.PortableViewModel.Test.Commands
{
    public class UnitTestCommand : ICommand
    {
        public UnitTestCommand()
        {
            Reset();
        }

        public bool CanExecuteResult { get; set; }

        public object LastParameter { get; private set; }

        public bool CanExecuteCalled { get; private set; }
        public bool ExecuteCalled { get; private set; }

        public void Reset()
        {
            CanExecuteResult = false;

            LastParameter = null;

            CanExecuteCalled = false;
            ExecuteCalled = false;
        }

        public void RaiseCanExecuteChanged()
        {
            var canExecuteChanged = CanExecuteChanged;

            if (canExecuteChanged != null)
            {
                canExecuteChanged(this, EventArgs.Empty);
            }
        }

        #region ICommand Members

        public event EventHandler CanExecuteChanged = null;

        public bool CanExecute(object parameter)
        {
            LastParameter = parameter;
            CanExecuteCalled = true;

            return CanExecuteResult;
        }

        public void Execute(object parameter)
        {
            LastParameter = parameter;

            ExecuteCalled = true;
        }

        #endregion
    }
}
