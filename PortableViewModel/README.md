# README #

#### MyGet Feed ####

| Package    | develop | beta | master |
|------------|---------|------|--------|
| `Acrotech.PortableViewModel` | [![MyGet](https://img.shields.io/myget/acrotech-develop/vpre/Acrotech.PortableViewModel.svg?style=flat-square)](https://www.myget.org/gallery/acrotech-develop) <br/> [![MyGet](https://img.shields.io/myget/acrotech-develop/dt/Acrotech.PortableViewModel.svg?style=flat-square)](https://www.myget.org/gallery/acrotech-develop) | [![MyGet](https://img.shields.io/myget/acrotech-beta/vpre/Acrotech.PortableViewModel.svg?style=flat-square)](https://www.myget.org/gallery/acrotech-beta) <br/> [![MyGet](https://img.shields.io/myget/acrotech-beta/dt/Acrotech.PortableViewModel.svg?style=flat-square)](https://www.myget.org/gallery/acrotech-beta) | [![MyGet](https://img.shields.io/myget/acrotech/v/Acrotech.PortableViewModel.svg?style=flat-square)](https://www.myget.org/gallery/acrotech) <br/> [![MyGet](https://img.shields.io/myget/acrotech/dt/Acrotech.PortableViewModel.svg?style=flat-square)](https://www.myget.org/gallery/acrotech) |

#### NuGet Feed ####

| Package | beta | master | downloads |
|---------|------|--------|-----------|
| `Acrotech.PortableViewModel` | [![NuGet](https://img.shields.io/nuget/vpre/Acrotech.PortableViewModel.svg?style=flat-square)](https://www.nuget.org/packages/Acrotech.PortableViewModel) | [![NuGet](https://img.shields.io/nuget/v/Acrotech.PortableViewModel.svg?style=flat-square)](https://www.nuget.org/packages/Acrotech.PortableViewModel) | [![NuGet](https://img.shields.io/nuget/dt/Acrotech.PortableViewModel.svg?style=flat-square)](https://www.nuget.org/packages/Acrotech.PortableViewModel) |

## About ##

This library is intended to provide the most minimal [portable view model](https://bitbucket.org/Acrotech/acrotech.portableviewmodel) possible, providing only a basic property notifier class with simple methods to generate those notifications. Also included are some very basic delegate command classes and a portable implementation of `ICommand`.

***NOTE***: `System.Windows.Input.ICommand` is not fully portable due to how **SL5** and **WP8** interpret it. In order to be truly portable, this library mocks the ICommand interface and provides minimal extensions to support `System.Windows.Input.ICommand` in either **SL5** mode or **NoSL5** mode. If you have no intent or targetting **SL5** then you would want to use the **NoSL5** package and if are only targetting **SL5** then you would want to use the **SL5** package. If you plan on targetting all platforms, then you must use the base package and make use of the **WPF.NET45** (and similar) packages to provide portable `ICommand` binding to standard controls. The **NoSL5** package targets profile 259, which means you cannot reference the package with any univerally portable libraries.

## Usage Notes ##

Assign the `TaskBaseViewModel.DefaultUITaskScheduler` and the `DelegateCommand.DefaultUITaskScheduler` static properties during application initialization to consolidate all UI scheduling onto a single task scheduler.

```csharp
var uiScheduler = TaskScheduler.FromCurrentSynchronizationContext();
TaskBaseViewModel.DefaultUITaskScheduler = uiScheduler;
DelegateCommand.DefaultUITaskScheduler = uiScheduler;
```

## Changelog ##

#### 2.0.0 ####

* Project cleanup and prep for new commanding add-on
* Move to single repository
* Enabled UI scheduling of `DelegateCommand` `CanExecuteChanged` events

#### 1.0.1.0 ####

* Added Acrotech.Versioning for versioning
* TaskBaseViewModel can now return "Empty Completed" Tasks instead of null
* Improved snippets
* Improved OnPropertyChanged function chaining (more options, allows args creation delegates)
* Refactored main RaiseAndSetIfChanged logic to extension methods (supports using extensions with another INotifyPropertyChanged view model framework)
* Usings cleanup
* Fully unit tested!
* Adding CF35 support
* Updating PCL profile to 344 (maximum compatibility)
* Improved the task scheduling version of the base view model (now allows scheduling tasks with results)
* Added nullable OnChanging delegate to RaiseAndSetIfChanged calls (called before assignment and event generation)
* adding delay and cancellation token options to threading functions

#### 1.0.0.3 ####

* Fixing bug with deprecated RaiseAndSetIfChanged where a null onChanged delegate would cause a NullReferenceException
    * Also fixing an ambiguous call compile time issue between the standard and deprecated functions

#### 1.0.0.2 ####

* Adding the TaskBaseViewModel, which supports the TaskScheduler to invoke property changes (or other delegates) on the UI thread
    * Note that this may not fully work properly yet, this is experimental at the moment
* Source code now contains Snippets for creating view model properties (still need to figure out how to include this via NuGet, so if you want to use them get them at the source repository)
    * You can place the snippets in %USERPROFILE%\Documents\Visual Studio 2012\Code Snippets\Visual C#\My Code Snippets

#### 1.0.0.1 ####

* On Changed delegate of RaiseAndSetIfChanged now provides the old property value as a delegate parameter (no parameter is still allowed for compatibility purposes)

#### 1.0.0.0 ####

* Initial Release
