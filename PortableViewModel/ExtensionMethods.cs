﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Reflection;
using System.Runtime.CompilerServices;

namespace Acrotech.PortableViewModel
{
    /// <summary>
    /// Extension methods to support performing RaiseAndSetIfChanged and PerformPropertyChange calls on any view model framework that implements INotifyPropertyChanged
    /// </summary>
    public static partial class ExtensionMethods
    {
        #region RaiseAndSetIfChanged

#if PocketPC
        // These extra PocketPC functions remove the onChanged and onChanging delegates, for simplicity

        public static T RaiseAndSetIfChanged<T>(
            this INotifyPropertyChanged source, 
            PropertyChangedEventHandler handler, 
            ref T backingField, 
            T newValue, 
            string propertyName
        )
        {
            return RaiseAndSetIfChanged(source, handler, ref backingField, newValue, propertyName, (Action<T>)null, (Action<T>)null, (Action<PropertyChangedEventHandler, INotifyPropertyChanged, Func<PropertyChangedEventArgs>>)null);
        }

        public static T RaiseAndSetIfChanged<T>(
            this INotifyPropertyChanged source, 
            PropertyChangedEventHandler handler, 
            ref T backingField, 
            T newValue, 
            PropertyChangedEventArgs args
        )
        {
            return RaiseAndSetIfChanged(source, handler, ref backingField, newValue, args, (Action<T>)null, (Action<T>)null, (Action<PropertyChangedEventHandler, INotifyPropertyChanged, Func<PropertyChangedEventArgs>>)null);
        }

        public static T RaiseAndSetIfChanged<T>(
            this INotifyPropertyChanged source, 
            PropertyChangedEventHandler handler, 
            ref T backingField, 
            T newValue, 
            Func<PropertyChangedEventArgs> argsCreator
        )
        {
            return RaiseAndSetIfChanged(source, handler, ref backingField, newValue, argsCreator, (Action<T>)null, (Action<T>)null, (Action<PropertyChangedEventHandler, INotifyPropertyChanged, Func<PropertyChangedEventArgs>>)null);
        }

        // These extra PocketPC functions remove the onChanging delegate, for simplicity

        public static T RaiseAndSetIfChanged<T>(
            this INotifyPropertyChanged source, 
            PropertyChangedEventHandler handler, 
            ref T backingField, 
            T newValue, 
            string propertyName, 
            Action<T> onChanged
        )
        {
            return RaiseAndSetIfChanged(source, handler, ref backingField, newValue, propertyName, onChanged, (Action<T>)null, (Action<PropertyChangedEventHandler, INotifyPropertyChanged, Func<PropertyChangedEventArgs>>)null);
        }

        public static T RaiseAndSetIfChanged<T>(
            this INotifyPropertyChanged source, 
            PropertyChangedEventHandler handler, 
            ref T backingField, 
            T newValue, 
            PropertyChangedEventArgs args, 
            Action<T> onChanged
        )
        {
            return RaiseAndSetIfChanged(source, handler, ref backingField, newValue, args, onChanged, (Action<T>)null, (Action<PropertyChangedEventHandler, INotifyPropertyChanged, Func<PropertyChangedEventArgs>>)null);
        }

        public static T RaiseAndSetIfChanged<T>(
            this INotifyPropertyChanged source, 
            PropertyChangedEventHandler handler, 
            ref T backingField, 
            T newValue, 
            Func<PropertyChangedEventArgs> argsCreator, 
            Action<T> onChanged
        )
        {
            return RaiseAndSetIfChanged(source, handler, ref backingField, newValue, argsCreator, onChanged, (Action<T>)null, (Action<PropertyChangedEventHandler, INotifyPropertyChanged, Func<PropertyChangedEventArgs>>)null);
        }
#endif

#if !PocketPC
        /// <summary>
        /// Helper function to simplify property changes and subsequent notifications by combining all code execution into a single function call. Automatic value equality comparison and dynamic change notification are built-in.
        /// </summary>
        /// <typeparam name="T">Changed property type</typeparam>
        /// <param name="source">View model that generated the property change notification</param>
        /// <param name="handler">Event handler that will produce the notification</param>
        /// <param name="backingField">Reference to the changed property backing field</param>
        /// <param name="newValue">Changed property's new value</param>
        /// <param name="onChanged">On Changed delegate to execute</param>
        /// <param name="onChanging">On Changing delegate to execute (before assignment)</param>
        /// <param name="notifyAction">Action that will perform the notification (defaults to PerformPropertyChange)</param>
        /// <param name="propertyName">Name of the property (Autoset with CallerMemberName)</param>
        /// <returns>The resulting value of the property</returns>
        /// <remarks>This function simply moves the propertyName to the end to simplify the call signature when allowing CallerMemberName to auto-detect the property name</remarks>
        public static T RaiseAndSetIfChanged<T>(
            this INotifyPropertyChanged source, 
            PropertyChangedEventHandler handler, 
            ref T backingField, 
            T newValue, 
            Action<T> onChanged = null, 
            Action<T> onChanging = null, 
            Action<PropertyChangedEventHandler, INotifyPropertyChanged, Func<PropertyChangedEventArgs>> notifyAction = null,
            [CallerMemberName] string propertyName = null
        )
        {
            return RaiseAndSetIfChanged(source, handler, ref backingField, newValue, propertyName, onChanged, onChanging, notifyAction);
        }
#endif

        /// <summary>
        /// Helper function to simplify property changes and subsequent notifications by combining all code execution into a single function call. Automatic value equality comparison and dynamic change notification are built-in.
        /// </summary>
        /// <typeparam name="T">Changed property type</typeparam>
        /// <param name="source">View model that generated the property change notification</param>
        /// <param name="handler">Event handler that will produce the notification</param>
        /// <param name="backingField">Reference to the changed property backing field</param>
        /// <param name="newValue">Changed property's new value</param>
        /// <param name="propertyName">Name of the property</param>
        /// <param name="onChanged">On Changed delegate to execute</param>
        /// <param name="onChanging">On Changing delegate to execute (before assignment)</param>
        /// <param name="notifyAction">Action that will perform the notification (defaults to PerformPropertyChange)</param>
        /// <returns>The resulting value of the property</returns>
#if PocketPC
        public static T RaiseAndSetIfChanged<T>(
            this INotifyPropertyChanged source, 
            PropertyChangedEventHandler handler, 
            ref T backingField, 
            T newValue, 
            string propertyName, 
            Action<T> onChanged, 
            Action<T> onChanging,
            Action<PropertyChangedEventHandler, INotifyPropertyChanged, Func<PropertyChangedEventArgs>> notifyAction
        )
#else
        public static T RaiseAndSetIfChanged<T>(
            this INotifyPropertyChanged source, 
            PropertyChangedEventHandler handler, 
            ref T backingField, 
            T newValue, 
            string propertyName, 
            Action<T> onChanged = null, 
            Action<T> onChanging = null,
            Action<PropertyChangedEventHandler, INotifyPropertyChanged, Func<PropertyChangedEventArgs>> notifyAction = null
        )
#endif
        {
            return RaiseAndSetIfChanged(source, handler, ref backingField, newValue, () => new PropertyChangedEventArgs(propertyName), onChanged, onChanging, notifyAction);
        }

        /// <summary>
        /// Helper function to simplify property changes and subsequent notifications by combining all code execution into a single function call. Automatic value equality comparison and dynamic change notification are built-in.
        /// </summary>
        /// <typeparam name="T">Changed property type</typeparam>
        /// <param name="source">View model that generated the property change notification</param>
        /// <param name="handler">Event handler that will produce the notification</param>
        /// <param name="backingField">Reference to the changed property backing field</param>
        /// <param name="newValue">Changed property's new value</param>
        /// <param name="args">Property change args</param>
        /// <param name="onChanged">On Changed delegate to execute</param>
        /// <param name="onChanging">On Changing delegate to execute (before assignment)</param>
        /// <param name="notifyAction">Action that will perform the notification (defaults to PerformPropertyChange)</param>
        /// <returns>The resulting value of the property</returns>
#if PocketPC
        public static T RaiseAndSetIfChanged<T>(
            this INotifyPropertyChanged source, 
            PropertyChangedEventHandler handler, 
            ref T backingField, 
            T newValue, 
            PropertyChangedEventArgs args, 
            Action<T> onChanged, 
            Action<T> onChanging,
            Action<PropertyChangedEventHandler, INotifyPropertyChanged, Func<PropertyChangedEventArgs>> notifyAction
        )
#else
        public static T RaiseAndSetIfChanged<T>(
            this INotifyPropertyChanged source, 
            PropertyChangedEventHandler handler, 
            ref T backingField, 
            T newValue, 
            PropertyChangedEventArgs args, 
            Action<T> onChanged = null, 
            Action<T> onChanging = null,
            Action<PropertyChangedEventHandler, INotifyPropertyChanged, Func<PropertyChangedEventArgs>> notifyAction = null
        )
#endif
        {
            return RaiseAndSetIfChanged(source, handler, ref backingField, newValue, () => args, onChanged, onChanging, notifyAction);
        }

        /// <summary>
        /// Helper function to simplify property changes and subsequent notifications by combining all code execution into a single function call. Automatic value equality comparison and dynamic change notification are built-in.
        /// </summary>
        /// <typeparam name="T">Changed property type</typeparam>
        /// <param name="source">View model that generated the property change notification</param>
        /// <param name="handler">Event handler that will produce the notification</param>
        /// <param name="backingField">Reference to the changed property backing field</param>
        /// <param name="newValue">Changed property's new value</param>
        /// <param name="argsCreator">Delegate to create the property change args</param>
        /// <param name="onChanged">On Changed delegate to execute</param>
        /// <param name="onChanging">On Changing delegate to execute (before assignment)</param>
        /// <param name="notifyAction">Action that will perform the notification (defaults to PerformPropertyChange)</param>
        /// <returns>The resulting value of the property</returns>
#if PocketPC
        public static T RaiseAndSetIfChanged<T>(
            this INotifyPropertyChanged source, 
            PropertyChangedEventHandler handler, 
            ref T backingField, 
            T newValue, 
            Func<PropertyChangedEventArgs> argsCreator, 
            Action<T> onChanged, 
            Action<T> onChanging,
            Action<PropertyChangedEventHandler, INotifyPropertyChanged, Func<PropertyChangedEventArgs>> notifyAction
        )
#else
        public static T RaiseAndSetIfChanged<T>(
            this INotifyPropertyChanged source, 
            PropertyChangedEventHandler handler, 
            ref T backingField, 
            T newValue, 
            Func<PropertyChangedEventArgs> argsCreator, 
            Action<T> onChanged = null, 
            Action<T> onChanging = null, 
            Action<PropertyChangedEventHandler, INotifyPropertyChanged, Func<PropertyChangedEventArgs>> notifyAction = null
        )
#endif
        {
            if (source != null && EqualityComparer<T>.Default.Equals(backingField, newValue) == false)
            {
                if (onChanging != null)
                {
                    onChanging(newValue);
                }

                var oldValue = backingField;

                backingField = newValue;

                (notifyAction ?? PerformPropertyChangeNotification).Invoke(handler, source, argsCreator);

                if (onChanged != null)
                {
                    onChanged(oldValue);
                }
            }

            return newValue;
        }

        #endregion

        #region PerformPropertyChangeNotification

        /// <summary>
        /// Performs a property change notification using the provided <paramref name="handler"/>, <paramref name="source"/>, and <paramref name="propertyName"/> 
        /// when <paramref name="handler"/> is non-null.
        /// </summary>
        /// <param name="handler">Event handler that will produce the notification</param>
        /// <param name="source">View model that generated the property change notification</param>
        /// <param name="propertyName">Name of the property</param>
        public static void PerformPropertyChangeNotification(this PropertyChangedEventHandler handler, INotifyPropertyChanged source, string propertyName)
        {
            PerformPropertyChangeNotification(handler, source, () => new PropertyChangedEventArgs(propertyName));
        }

        /// <summary>
        /// Performs a property change notification using the provided <paramref name="handler"/>, <paramref name="source"/>, and <paramref name="args"/> 
        /// when <paramref name="handler"/> is non-null.
        /// </summary>
        /// <param name="handler">Event handler that will produce the notification</param>
        /// <param name="source">View model that generated the property change notification</param>
        /// <param name="args">Property change args</param>
        public static void PerformPropertyChangeNotification(this PropertyChangedEventHandler handler, INotifyPropertyChanged source, PropertyChangedEventArgs args)
        {
            PerformPropertyChangeNotification(handler, source, () => args);
        }

        /// <summary>
        /// Performs a property change notification using the provided <paramref name="handler"/>, <paramref name="source"/>, and <paramref name="argsCreator"/> 
        /// when <paramref name="handler"/> is non-null.
        /// </summary>
        /// <param name="handler">Event handler that will produce the notification</param>
        /// <param name="source">View model that generated the property change notification</param>
        /// <param name="argsCreator">Delegate to create the property change args</param>
        public static void PerformPropertyChangeNotification(this PropertyChangedEventHandler handler, INotifyPropertyChanged source, Func<PropertyChangedEventArgs> argsCreator)
        {
            if (handler != null)
            {
                handler(source, argsCreator == null ? null : argsCreator());
            }
        }

        #endregion
    }
}
