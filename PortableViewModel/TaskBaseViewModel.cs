﻿using System;
using System.ComponentModel;
using System.Threading;
using System.Threading.Tasks;

namespace Acrotech.PortableViewModel
{
    /// <summary>
    /// Extension to the base view model that additionally provides simplified access to Task based multi threading
    /// </summary>
    /// <remarks>This class is experimental</remarks>
    public abstract class TaskBaseViewModel : BaseViewModel
    {
        /// <summary>
        /// Static readonly empty Task that is already completed
        /// </summary>
        /// <remarks>This is returned instead of null so that task chaining does not suffer</remarks>
        public static readonly Task EmptyCompletedTask = CreateEmptyCompletedTask<object>();

        /// <summary>
        /// Create a new instance of the view model using the Current Synchronization Context for the Scheduler (or the static UI Scheduler if it has been set)
        /// </summary>
        protected TaskBaseViewModel()
            : this(DefaultUITaskScheduler ?? TaskScheduler.FromCurrentSynchronizationContext())
        {
        }

        /// <summary>
        /// Create a new instance of the view model using the provided <paramref name="uiScheduler"/>
        /// </summary>
        /// <param name="uiScheduler">Scheduler to use when executing on the UI thread</param>
        protected TaskBaseViewModel(TaskScheduler uiScheduler)
        {
            UIScheduler = uiScheduler;
        }

        /// <summary>
        /// Static Scheduler storage for the default UI Scheduler
        /// </summary>
        public static TaskScheduler DefaultUITaskScheduler { get; set; }

        /// <summary>
        /// Scheduler to use when executing on the UI thread
        /// </summary>
        public TaskScheduler UIScheduler { get; private set; }

        /// <summary>
        /// Creates an empty completed task with the specified <paramref name="result"/>
        /// </summary>
        /// <typeparam name="T">Type of result</typeparam>
        /// <param name="result">Task's predefined result</param>
        /// <returns>A completed task with the specified <paramref name="result"/></returns>
        public static Task<T> CreateEmptyCompletedTask<T>(T result = default(T))
        {
            var source = new TaskCompletionSource<T>();
            source.SetResult(result);
            return source.Task;
        }

        private void CheckForNullUIScheduler()
        {
            if (UIScheduler == null)
            {
                throw new InvalidOperationException("UI Scheduler is null");
            }
        }

        /// <summary>
        /// Run the <paramref name="action"/> on the UI thread
        /// </summary>
        /// <param name="action">Action to execute</param>
        /// <param name="delay">Milliseconds to delay before running <paramref name="action"/></param>
        /// <param name="cancellationToken">Cancellation Token (Defaults to None)</param>
        /// <returns>Task that runs the <paramref name="action"/></returns>
        protected virtual Task RunOnUiThread(Action action, int delay = 0, CancellationToken? cancellationToken = null)
        {
            CheckForNullUIScheduler();

            return StartTask(action, cancellationToken, scheduler: UIScheduler, delay: delay);
        }

        /// <summary>
        /// Run the <paramref name="action"/> on the UI thread and expect a result of type <typeparamref name="T"/>
        /// </summary>
        /// <typeparam name="T">Type of expected result</typeparam>
        /// <param name="action">Action to execute</param>
        /// <param name="delay">Milliseconds to delay before running <paramref name="action"/></param>
        /// <param name="cancellationToken">Cancellation Token (Defaults to None)</param>
        /// <returns>Task that runs the <paramref name="action"/></returns>
        protected virtual Task<T> RunOnUiThread<T>(Func<T> action, int delay = 0, CancellationToken? cancellationToken = null)
        {
            CheckForNullUIScheduler();

            return StartTask(action, cancellationToken, scheduler: UIScheduler, delay: delay);
        }

        /// <summary>
        /// Run the <paramref name="action"/> off the UI thread
        /// </summary>
        /// <param name="action">Action to execute</param>
        /// <param name="delay">Milliseconds to delay before running <paramref name="action"/></param>
        /// <param name="cancellationToken">Cancellation Token (Defaults to None)</param>
        /// <returns>Task that runs the <paramref name="action"/></returns>
        protected virtual Task RunOffUiThread(Action action, int delay = 0, CancellationToken? cancellationToken = null)
        {
            return StartTask(action, cancellationToken, delay: delay);
        }

        /// <summary>
        /// Run the <paramref name="action"/> off the UI thread and expect a result of type <typeparamref name="T"/>
        /// </summary>
        /// <typeparam name="T">Type of expected result</typeparam>
        /// <param name="action">Action to execute</param>
        /// <param name="delay">Milliseconds to delay before running <paramref name="action"/></param>
        /// <param name="cancellationToken">Cancellation Token (Defaults to None)</param>
        /// <returns>Task that runs the <paramref name="action"/></returns>
        protected virtual Task<T> RunOffUiThread<T>(Func<T> action, int delay = 0, CancellationToken? cancellationToken = null)
        {
            return StartTask(action, cancellationToken, delay: delay);
        }

        /// <summary>
        /// Start a task using the provided <paramref name="action"/> and additional creation parameters
        /// </summary>
        /// <param name="action">Action to execute</param>
        /// <param name="cancellationToken">Cancellation Token (Defaults to None)</param>
        /// <param name="scheduler">Scheduler (Defaults to Default)</param>
        /// <param name="delay">Delay in milliseconds before action is executed (must be >= -1)</param>
        /// <param name="continuationOptions">Continuation Options applied to action task (Defaults to OnlyOnRanToCompletion)</param>
        /// <returns>Task that runs the <paramref name="action"/></returns>
        /// <remarks>This function always returns a valid task (which may be an already completed empty task)</remarks>
        public static Task StartTask(Action action, CancellationToken? cancellationToken = null, TaskScheduler scheduler = null, int delay = 0, 
            TaskContinuationOptions continuationOptions = TaskContinuationOptions.OnlyOnRanToCompletion)
        {
            var task = EmptyCompletedTask;

            if (action != null)
            {
                task = TaskEx.Delay(delay, cancellationToken ?? CancellationToken.None)
                    .ContinueWith(_ => action(), cancellationToken ?? CancellationToken.None, continuationOptions, scheduler ?? TaskScheduler.Default);
            }

            return task;
        }

        /// <summary>
        /// Start a task using the provided <paramref name="action"/> and additional creation parameters and expect a result of type <typeparamref name="T"/>
        /// </summary>
        /// <typeparam name="T">Type of expected result</typeparam>
        /// <param name="action">Action to execute</param>
        /// <param name="cancellationToken">Cancellation Token (Defaults to None)</param>
        /// <param name="scheduler">Scheduler (Defaults to Default)</param>
        /// <param name="delay">Delay in milliseconds before action is executed (must be >= -1)</param>
        /// <param name="continuationOptions">Continuation Options applied to action task (Defaults to OnlyOnRanToCompletion)</param>
        /// <returns>Task that runs the <paramref name="action"/></returns>
        /// <remarks>This function always returns a valid task (which may be an already completed empty task)</remarks>
        public static Task<T> StartTask<T>(Func<T> action, CancellationToken? cancellationToken = null, TaskScheduler scheduler = null, int delay = 0,
            TaskContinuationOptions continuationOptions = TaskContinuationOptions.OnlyOnRanToCompletion)
        {
            var task = CreateEmptyCompletedTask<T>();

            if (action != null)
            {
                task = TaskEx.Delay(delay, cancellationToken ?? CancellationToken.None)
                    .ContinueWith(_ => action(), cancellationToken ?? CancellationToken.None, continuationOptions, scheduler ?? TaskScheduler.Default);
            }

            return task;
        }

        /// <inheritdoc/>
        /// <remarks>Handler is invoked on the UI thread</remarks>
        protected override void OnPropertyChanged(PropertyChangedEventHandler handler, Func<PropertyChangedEventArgs> argsCreator)
        {
            OnUiPropertyChanged(handler, argsCreator);
        }

        /// <summary>
        /// Creates a task and invokes the base OnPropertyChanged function on the UI thread.
        /// </summary>
        /// <param name="handler">Notification handler to invoke</param>
        /// <param name="argsCreator">Property change args (creation delegate)</param>
        /// <returns>Task responsible for the function invocation</returns>
        protected virtual Task OnUiPropertyChanged(PropertyChangedEventHandler handler, Func<PropertyChangedEventArgs> argsCreator)
        {
            Task task = EmptyCompletedTask;

            if (handler != null)
            {
                task = RunOnUiThread(() => base.OnPropertyChanged(handler, argsCreator));
            }

            return task;
        }
    }
}
