# README #

| Host  | develop | beta | master |
|-------|---------|------|--------|
| MyGet | [![MyGet](https://www.myget.org/BuildSource/Badge/acrotech-develop?identifier=0be0a405-ff73-4177-b63b-a2155859e6cc)](https://www.myget.org/gallery/acrotech-develop) | [![MyGet](https://www.myget.org/BuildSource/Badge/acrotech-beta?identifier=83851fa6-ee62-46ef-8057-f87bd74c6115)](https://www.myget.org/gallery/acrotech-beta) | [![MyGet](https://www.myget.org/BuildSource/Badge/acrotech?identifier=b016077e-c2fe-4568-8a76-94b07f40e847)](https://www.myget.org/gallery/acrotech) |

## About ##

This project was created to solve a very common problem, to provide a protable, simple, lightweight, and unobtrusive view model framework. There are **PLENTY** of view model frameworks out there, many of them meet some of the goals of this library, but nearly all fall short on the light weight goal. This library does not try and force you to integrate with custom fancy components to simplify your view models. Instead, this library tries to provide what it can through **extension methods** and **snippets**, with only the most basic code being provided by the base classes. Due to this architecture decision, the use of this library does not even necessitate using the base classes as the extension methods operate on `INotifyPropertyChanged`. This design allows you to decide if you would like to take advantage of the base classes or simply take advantage of the extensions.

This library provides only a few classes:

* `BaseViewModel`: base implementation of an `INotifyPropertyChanged`.
* `TaskBaseViewModel`: `BaseViewModel` with `Task` scheduling support.
* `ICommand`: replcation of `System.Windows.Input.ICommand` for portable use (*both generic and non-generic flavours*).
* `DelegateCommand`: routine delegate `ICommand` implementation (*both generic and non-generic flavours*).
* `CancelCommandArgs`: basic implementation for a *cancellable* command.
* `ExtensionMethods`: extension methods for both `INotifyPropertyChanged` and `ICommand`

### `BaseViewModel` ###

This class simply provides the `PropertyChanged` event from `INotifyPropertyChanged` and some `protected virtual void OnPropertyChanged` functions to simplify generation a property change notification. Additionally this class provides helper functions to simplify calling the `RaiseAndSetIfChanged` extension methods (by reducing the number of parameters required).

The `OnPropertyChanged` and `RaiseAndSetIfChanged` functions come in three flavours for capturing the property that changed:

0. A `string`
0. A `PropertyChangedEventArgs`
0. A `Func<PropertyChangedEventArgs>` (*args creator delegate*)

As a bonus, when using the `string` flavour, you may omit the `string` parameter and it will be automatically detected using the `[CallerMemberName]` attribute. ***NOTE*** that this only works when you call either function from the **property's setter**.

Both of these function sets work by chaining down to a single (most significant) function, those that accept an `argsCreator` delegate. If you override these functions in a sub-class then you will be able to hook into all calls to that function set. The `argsCreator` functions both simply call into the corresponding extension methods (`PerformPropertyChangeNotification` for `OnPropertyChanged`). What this means is that there is no actual logic in this base class, other than what is required to chain the functions together, keeping the class **ULTRA** light weight.

The snippets provide easy to use macros that expand to consistent view model properties. The snippets are designed to work with the `BaseViewModel` but could easily be adapted to work with the extension methods instead. These snippets are designed to be a solid default, but to be customized to the style and function of the developer. The snippets are discussed in more detail below.

### `TaskBaseViewModel` ###

Extending the `BaseViewModel`, this class additionally provides a method of scheduling actions on and off the UI thread. The constructor allows specifying explicitly the UI `TaskScheduler` or by default it will fetch the scheduler from `TaskScheduler.FromCurrentSynchronizationContext()`. `OnPropertyChanged` from the `BaseViewModel` is overridden to instead call `OnUiPropertyChanged`, which will schedule the notification to occur on the UI thread (by calling `base.OnPropertyChanged(...)`). This means that all property notifications will transparently be scheduled on the UI thread. `OnUiPropertyChanged` can be overridden to manage the notification tasks that are scheduled (if desired). ***NOTE*** that notifications are not scheduled if there are no listeners on the `PropertyChanged` event.

This class also provides easy scheduling of actions on and off the UI thread using the `RunOnUiThread` and `RunOffUiThread` functions. Both functions allow result and no result based actions, and simply call into the respective `StartTask` functions. These `StartTask` functions allow more control over creating and scheduling tasks by allowing a cancellation token to be supplied, the scheduler to be supplied, a start delay to be specified, and the task continuation options to be customized.

***NOTE***: You may provide the `TaskBaseViewModel.DefaultUITaskScheduler` with a meaningful value in order to default to that scheduler instead of creating a new scheduler with `TaskScheduler.FromCurrentSynchronizationContext()`. This is recommended for larger products, as `TaskScheduler.FromCurrentSynchronizationContext()` creates a new scheduler every time.

#### `StartTask` ####

This function will **ALWAYS** return a valid `Task`. In some cases it may return an already completed task (if your action happens to be null). Given a valid action, all tasks will begin with a delay task (which defaults to no delay at all) and continue with scheduling a task to run the provided action. If no scheduler is provided then the `TaskScheduler.Default` is used. With this logic, you may always `await` on a `Task` created with `StartTask`.

### `ICommand` ###

This interface simply provides a mirror to the `System.Windows.Input.ICommand` interface. In addition, there is a generic version available that allows strongly typed command parameters (instead of `object`). The purpose of these interfaces is to provide a portable contract on commanding.

### `DelegateCommand` ###

The `DelegateCommand`, as most others available, provides an `ICommand` implementation using delegates for `Execute` and `CanExecute`. A `RaiseCanExecuteChanged` function exists to invoke the `CanExecuteChanged` event from an external source. The `CanExecute` delegate is optional, and if not provided will default to always returning `true`. If the `Execute` delegate is (for some reason or other) `null`, the delegate will not be executed (and therefore not throw an exception). This `DelegateCommand` allows providing parameterless delegates, when this type of command is instantiated, the parameter will be transparently sent in as `null`. `DelegateCommand` supports UI scheduling by allowing a `TaskScheduler` constructor parameter, or more simply by setting the `DefaultUITaskScheduler` static property before instantiation(s).

The generic version of `DelegateCommand` simply performs the necessary type casts before invoking the delegates provided. This means that any invalid casting will result in an exception.

There are platform packages (`Acrotech.PortableViewModel.Commands.Platform.*`) that may be used to produce commands that adhere to the `System.Windows.Input.ICommand`, however these packages lead to less portable code and should be avoided if possible.

### `CancelCommandArgs` ###

This class provides a very simple method of producing a *cancellable* command. By using these args as the command parameter, the delegate can easily inspect if a cancellation was requested or not.

### `ExtensionMethods` ###

The extension methods classes provide all of the view model logic (what little there is). All view model extension operate on `INotifyPropertyChanged` instances. There are two sets of extension methods for view models, `RaiseAndSetIfChanged` and `PerformPropertyChangeNotification`. Additionally, there are two small extensions for `ICommand` that support calling `Execute` and `CanExecute` without providing a command paramter (which simply uses `null` as the parameter).

#### `RaiseAndSetIfChanged` ####

These extensions were inspired by the [RxUI](http://reactiveui.net/) work of [Paul Betts](http://www.twitter.com/xpaulbettsx). The purpose of these extensions is to simplify the ubiquitous task of performing a property value change and notification, such that it can be written with the bare minimum amount of code. All of these extensions chain down to the main extension method (which takes an `argsCreator` parameter), which means that calling any of the functions yields the same result. Thanks to some clever design work and the help of some new .NET compiler features, we can reduce this down to a very small single line of code:

```csharp
private string propText = default(string);
public string Text
{
  get { return propText; }

  // BaseViewModel Version
  set { RaiseAndSetIfChanged(ref propText, value); }

  // Extension Method Version
  set { this.RaiseAndSetIfChanged(PropertyChanged, ref propText, value); }
}
```

This code allows us to update our backing field, generate notifications, and hook into `OnChanged` and `OnChanging` delegates. It even allows us to hook into the delegate that performs the change notification (see the `TaskBaseViewModel` for an example of this, where the delegate is overriden to schedule notifications on the UI thread).

Using this pattern, we can shrink down view model code by stripping out repeatable value-less syntax. As you can see, the `BaseViewModel` version is slightly more compact due to certain parameters being implicitly sent to the extension method for you (such as the `INotifyPropertyChanged` source, and the `PropertyChangedEventHandler` event).

#### `PerformPropertyChangeNotification` ####

These extensions provide a unified and thread safe method of performing a property change notification. If the `handler` specified is null, the event invocation will not occur, this means that the `PropertyChangedEventArgs` will never be created which speeds up performance just a bit. If the `argsCreator` is `null`, then `null` args will be sent during the event invocation. Because these functions chain down to the primary (`argsCreator`) extension (similar to the `RaiseAndSetIfChanged` extension), a call to any of these extensions yields the same result.

## Instructions ##

Typically, you will want to subclass one of the base view model classes and use that class as your project's base view model. It is recommended that you use the TaskBaseViewModel as it will save you grief down the road when you have to deal with UI scheduling. However, for single threaded applications, the `BaseViewModel` will perform just fine.

Once you have your base view model class set up and ready to go, you will want to install the snippets provided with this package. These snippets can be installed in your `My Code Snippets` folder (i.e., `%USERPROFILE%\Documents\Visual Studio 2013\Code Snippets\Visual C#\My Code Snippets`). Once installed, you can use the snippets like any other, start typing the shortcut sequence (i.e., `vmprop`) and tab through the completions. There are four view model snippets, and two commanding snippets.

#### View Model Snippets ####

* `vmprop`: Standard view model property.
* `vmproppc`: Standard view model property with an `OnChanged` delegate setup.
* `vmpropnbf`: View model property without a backing field.
* `vmpropnbfpc`: View model property without a backing field, but with an `OnChanged` delegate that allows you to update the data source.

Typically you will be using the `vmprop` or `vmproppc` snippets in almost all cases. In some cases you may want to use `vmpropnbfpc` so that you can fetch your view model property from another data source. `vmpropnbf` is simply there to provide a field-less snippet that you can customize to your needs. ***NOTE*** that with `vmpropnbf` and `vmpropnbfpc` the standard view model extensions are not used since there is no backing field.

All view model snippets provide a `const string` containing the name of the property. This allows you to check the `PropertyChangedEventArgs.PropertyName` value against a strongly typed constant.

#### Commanding Snippets ####

* `vmcmd`: Standard view model command.
* `vmcmdnce`: View model command without a `CanExecute` delegate.

The delegate command snippets are fairly straight forward. The command is instantiated lazily and the delegate function implementations are provided to expand upon.
