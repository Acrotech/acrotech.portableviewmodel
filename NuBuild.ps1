Prep-Project (Get-Path "PortableViewModel")
Prep-Project (Get-Path "Commands\SL5")
Prep-Project (Get-Path "Commands\NoSL5")
Prep-Project (Get-Path "Commands\WPF\NET45")

Build (Get-Path "PortableViewModel") "PortableViewModel.csproj"
Build (Get-Path "Commands\SL5") "Commands.SL5.csproj"
Build (Get-Path "Commands\NoSL5") "Commands.NoSL5.csproj"
Build (Get-Path "Commands\WPF\NET45") "Commands.WPF.NET45.csproj"

Build-Project (Get-Path "Test\Test.csproj")
Run-Tests (Get-Path "Test\bin\$env:Configuration\Acrotech.PortableViewModel.Test.dll")

# Until NuGet 3.0.0 is released (and deployed to MyGet and other build services), we need to use the patched NuGet stored locally.
# this is a workaround to packing PCL libraries with replacement tokens in the nuspec
$env:NuGetExe = (Get-Path ".nuget\NuGet.exe")
Log-Build "Updated NuGetExe to $env:NuGetExe"

Pack-Packagable (Get-Path "PortableViewModel\PortableViewModel.csproj")
Pack-Packagable (Get-Path "Commands\SL5\Commands.SL5.csproj")
Pack-Packagable (Get-Path "Commands\NoSL5\Commands.NoSL5.csproj")
Pack-Packagable (Get-Path "Commands\WPF\NET45\Commands.WPF.NET45.csproj")
