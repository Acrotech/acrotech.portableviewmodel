# README #

#### MyGet Feed ####

| Package    | develop | beta | master |
|------------|---------|------|--------|
| `Acrotech.PortableViewModel.Commands.NoSL5` | [![MyGet](https://img.shields.io/myget/acrotech-develop/vpre/Acrotech.PortableViewModel.Commands.NoSL5.svg?style=flat-square)](https://www.myget.org/gallery/acrotech-develop) <br/> [![MyGet](https://img.shields.io/myget/acrotech-develop/dt/Acrotech.PortableViewModel.Commands.NoSL5.svg?style=flat-square)](https://www.myget.org/gallery/acrotech-develop) | [![MyGet](https://img.shields.io/myget/acrotech-beta/vpre/Acrotech.PortableViewModel.Commands.NoSL5.svg?style=flat-square)](https://www.myget.org/gallery/acrotech-beta) <br/> [![MyGet](https://img.shields.io/myget/acrotech-beta/dt/Acrotech.PortableViewModel.Commands.NoSL5.svg?style=flat-square)](https://www.myget.org/gallery/acrotech-beta) | [![MyGet](https://img.shields.io/myget/acrotech/v/Acrotech.PortableViewModel.Commands.NoSL5.svg?style=flat-square)](https://www.myget.org/gallery/acrotech) <br/> [![MyGet](https://img.shields.io/myget/acrotech/dt/Acrotech.PortableViewModel.Commands.NoSL5.svg?style=flat-square)](https://www.myget.org/gallery/acrotech) |
| `Acrotech.PortableViewModel.Commands.SL5` | [![MyGet](https://img.shields.io/myget/acrotech-develop/vpre/Acrotech.PortableViewModel.Commands.SL5.svg?style=flat-square)](https://www.myget.org/gallery/acrotech-develop) <br/> [![MyGet](https://img.shields.io/myget/acrotech-develop/dt/Acrotech.PortableViewModel.Commands.SL5.svg?style=flat-square)](https://www.myget.org/gallery/acrotech-develop) | [![MyGet](https://img.shields.io/myget/acrotech-beta/vpre/Acrotech.PortableViewModel.Commands.SL5.svg?style=flat-square)](https://www.myget.org/gallery/acrotech-beta) <br/> [![MyGet](https://img.shields.io/myget/acrotech-beta/dt/Acrotech.PortableViewModel.Commands.SL5.svg?style=flat-square)](https://www.myget.org/gallery/acrotech-beta) | [![MyGet](https://img.shields.io/myget/acrotech/v/Acrotech.PortableViewModel.Commands.SL5.svg?style=flat-square)](https://www.myget.org/gallery/acrotech) <br/> [![MyGet](https://img.shields.io/myget/acrotech/dt/Acrotech.PortableViewModel.Commands.SL5.svg?style=flat-square)](https://www.myget.org/gallery/acrotech) |

#### NuGet Feed ####

| Package | beta | master | downloads |
|---------|------|--------|-----------|
| `Acrotech.PortableViewModel.Commands.NoSL5` | [![NuGet](https://img.shields.io/nuget/vpre/Acrotech.PortableViewModel.Commands.NoSL5.svg?style=flat-square)](https://www.nuget.org/packages/Acrotech.PortableViewModel.Commands.NoSL5) | [![NuGet](https://img.shields.io/nuget/v/Acrotech.PortableViewModel.Commands.NoSL5.svg?style=flat-square)](https://www.nuget.org/packages/Acrotech.PortableViewModel.Commands.NoSL5) | [![NuGet](https://img.shields.io/nuget/dt/Acrotech.PortableViewModel.Commands.NoSL5.svg?style=flat-square)](https://www.nuget.org/packages/Acrotech.PortableViewModel.Commands.NoSL5) |
| `Acrotech.PortableViewModel.Commands.SL5` | [![NuGet](https://img.shields.io/nuget/vpre/Acrotech.PortableViewModel.Commands.SL5.svg?style=flat-square)](https://www.nuget.org/packages/Acrotech.PortableViewModel.Commands.SL5) | [![NuGet](https://img.shields.io/nuget/v/Acrotech.PortableViewModel.Commands.SL5.svg?style=flat-square)](https://www.nuget.org/packages/Acrotech.PortableViewModel.Commands.SL5) | [![NuGet](https://img.shields.io/nuget/dt/Acrotech.PortableViewModel.Commands.SL5.svg?style=flat-square)](https://www.nuget.org/packages/Acrotech.PortableViewModel.Commands.SL5) |

## About ##

These libraries extend the commanding component of the [PortableViewModel framework](https://bitbucket.org/Acrotech/acrotech.portableviewmodel) by providing the delegate commands with a `System.Windows.Input.ICommand` implementation. This allows routine command binding using built-in controls.

Additionally, the **WPF** packages provide simple methods of binding portable commands to existing WPF controls without requiring the use of the platform specific (**Commands.Platform.SL5** and **Commands.Platform.NoSL5**) classes. See the **WPF** `README` file for more details.

## Usage Notes ##

These classes are provided to fill the gap of a `PortableViewModel` `DelegateCommand` that implements `System.Windows.Input.ICommand`. This `ICommand` interface is not very portable and therefore should be avoided in portable applications. It is instead recommended that the **WPF** binding adapters be used with the fully portable `ICommand` interface.

## Changelog ##

#### 2.0.0 ####

* Major shift towards universal portability
    * Universal (Profile 344) commanding without support of System.Windows.Input.ICommand
    * SL5 and NoSL5 (SL5 and Profile 259) assemblies that individually support System.Windows.Input.ICommand
    * WPF package to support direct portable command binding

#### 1.0.1.0 ####

* Refactoring to support optional parameters
* Better handling of null delegates (no exceptions thrown)
* Usings cleanup
* Fully unit tested!
* Adding CF35 support
* Updating PCL profile to 259 (better compatibility)
    * Temporarily dropping SL5 support due to incompatibilty of ICommand with WP SL

#### 1.0.0.0 ####

* Initial Release
