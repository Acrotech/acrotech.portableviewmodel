# README #

#### MyGet Feed ####

| Package    | develop | beta | master |
|------------|---------|------|--------|
| `Acrotech.PortableViewModel.Commands.WPF.NET45` | [![MyGet](https://img.shields.io/myget/acrotech-develop/vpre/Acrotech.PortableViewModel.Commands.WPF.NET45.svg?style=flat-square)](https://www.myget.org/gallery/acrotech-develop) <br/> [![MyGet](https://img.shields.io/myget/acrotech-develop/dt/Acrotech.PortableViewModel.Commands.WPF.NET45.svg?style=flat-square)](https://www.myget.org/gallery/acrotech-develop) | [![MyGet](https://img.shields.io/myget/acrotech-beta/vpre/Acrotech.PortableViewModel.Commands.WPF.NET45.svg?style=flat-square)](https://www.myget.org/gallery/acrotech-beta) <br/> [![MyGet](https://img.shields.io/myget/acrotech-beta/dt/Acrotech.PortableViewModel.Commands.WPF.NET45.svg?style=flat-square)](https://www.myget.org/gallery/acrotech-beta) | [![MyGet](https://img.shields.io/myget/acrotech/v/Acrotech.PortableViewModel.Commands.WPF.NET45.svg?style=flat-square)](https://www.myget.org/gallery/acrotech) <br/> [![MyGet](https://img.shields.io/myget/acrotech/dt/Acrotech.PortableViewModel.Commands.WPF.NET45.svg?style=flat-square)](https://www.myget.org/gallery/acrotech) |

#### NuGet Feed ####

| Package | beta | master | downloads |
|---------|------|--------|-----------|
| `Acrotech.PortableViewModel.Commands.WPF.NET45` | [![NuGet](https://img.shields.io/nuget/vpre/Acrotech.PortableViewModel.Commands.WPF.NET45.svg?style=flat-square)](https://www.nuget.org/packages/Acrotech.PortableViewModel.Commands.WPF.NET45) | [![NuGet](https://img.shields.io/nuget/v/Acrotech.PortableViewModel.Commands.WPF.NET45.svg?style=flat-square)](https://www.nuget.org/packages/Acrotech.PortableViewModel.Commands.WPF.NET45) | [![NuGet](https://img.shields.io/nuget/dt/Acrotech.PortableViewModel.Commands.WPF.NET45.svg?style=flat-square)](https://www.nuget.org/packages/Acrotech.PortableViewModel.Commands.WPF.NET45) |

## About ##

These libraries extend the commanding component of the [PortableViewModel framework](https://bitbucket.org/Acrotech/acrotech.portableviewmodel) by providing the delegate commands with a simple WPF control binding method. This allows a minimal deviation from routine command binding using built-in controls.

The WPF packages are built for each platform from a shared code base and provide a converter, a portable command wrapper, and a custom portable command binding extension to simplify portable command binding in WPF layouts. Currently the following platforms are supported:

* **NET45**

If you would like to see another platform supported please contact me to set it up.

## Usage Notes ##

In the `XAML` files, import the portable command WPF adapter namespace to gain access to the portable command binding adapter markup extension.

```xml
<UserControl
  xmlns:pcmd="clr-namespace:Acrotech.PortableViewModel.Commands.WPF;assembly=Acrotech.PortableViewModel.Commands.WPF.NET45">
  <!-- ... -->
  <Button Command="{pcmd:PortableCommandBinding Path=ZoomOut}"/>
</UserControl>
```

## Changelog ##

#### 2.0.0 ####

* Initial Release
