﻿using System;
using System.Windows;
using System.Windows.Data;
using System.Windows.Markup;

namespace Acrotech.PortableViewModel.Commands.WPF
{
    /// <summary>
    /// A command binding helper that simplifies binding to a portable command by automatically 
    /// setting the binding Converter to the default PortableCommandBindingConverter
    /// </summary>
    public class PortableCommandBinding : Binding
    {
        /// <summary>
        /// Default constructor, requires at least explicitly setting Path property
        /// </summary>
        public PortableCommandBinding()
            : base()
        {
            SetDefaultBindingParameters();
        }

        /// <summary>
        /// Standard constructor
        /// </summary>
        /// <param name="path">The path for binding</param>
        public PortableCommandBinding(string path)
            : base(path)
        {
            SetDefaultBindingParameters();
        }

        /// <summary>
        /// Overloaded constructor to supply a custom converter
        /// </summary>
        /// <param name="path">The path for binding</param>
        /// <param name="converter">The converter to use</param>
        public PortableCommandBinding(string path, PortableCommandBindingConverter converter)
            : base(path)
        {
            SetDefaultBindingParameters(converter);
        }

        private void SetDefaultBindingParameters(PortableCommandBindingConverter converter = null)
        {
            Mode = BindingMode.OneTime;
            Converter = converter ?? PortableCommandBindingConverter.Default;
        }
    }
}
