﻿using System;
using System.Globalization;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Data;

namespace Acrotech.PortableViewModel.Commands.WPF
{
    /// <summary>
    /// This converter will wrap a Portable Command in a PortableCommandWrapper, 
    /// which implements the System.Windows.Input.ICommand interface.
    /// </summary>
    public class PortableCommandBindingConverter : IValueConverter
    {
        /// <summary>
        /// Default converter that uses the TaskScheduler from the current SynchronizationContext
        /// </summary>
        public static readonly PortableCommandBindingConverter Default = new PortableCommandBindingConverter();

        private PortableCommandBindingConverter()
            : this(TaskScheduler.FromCurrentSynchronizationContext())
        {
        }

        /// <summary>
        /// Overloaded constructor that allows specifying an explicit scheduler if created off the UI thread
        /// </summary>
        /// <param name="scheduler">Scheduler to use with the PortableCommandWrapper instances</param>
        public PortableCommandBindingConverter(TaskScheduler scheduler)
        {
            Scheduler = scheduler ?? TaskScheduler.FromCurrentSynchronizationContext();
        }

        /// <summary>
        /// Scheduler associated with this converter
        /// </summary>
        public TaskScheduler Scheduler { get; private set; }

        /// <inheritdoc/>
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var converted = value;

            if (value is ICommand)
            {
                converted = new PortableCommandWrapper((ICommand)value, Scheduler);
            }

            return converted;
        }

        /// <inheritdoc/>
        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var converted = value;

            if (value is PortableCommandWrapper)
            {
                converted = ((PortableCommandWrapper)value).PortableCommand;
            }

            return value;
        }
    }
}
