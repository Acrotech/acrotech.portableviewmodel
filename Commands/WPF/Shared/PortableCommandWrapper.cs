﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Acrotech.PortableViewModel.Commands.WPF
{
    /// <summary>
    /// This wrapper acts as a pass-thru System.Windows.Input.ICommand implementation of a Portable Command
    /// </summary>
    public class PortableCommandWrapper : System.Windows.Input.ICommand
    {
        /// <summary>
        /// Constructor to create a new wrapper with the specified Portable Command and scheduler
        /// </summary>
        /// <param name="portableCommand">Portable Command to wrap</param>
        /// <param name="scheduler">Scheduler to bubble CanExecuteChanged events on</param>
        public PortableCommandWrapper(ICommand portableCommand, TaskScheduler scheduler = null)
        {
            PortableCommand = portableCommand;
            Scheduler = scheduler ?? TaskScheduler.FromCurrentSynchronizationContext();

            PortableCommand.CanExecuteChanged += PortableCommand_CanExecuteChanged;
        }

        /// <summary>
        /// Portable Command being wrapped
        /// </summary>
        public ICommand PortableCommand { get; private set; }

        /// <summary>
        /// Scheduler associated with this wrapper
        /// </summary>
        public TaskScheduler Scheduler { get; private set; }

        private void PortableCommand_CanExecuteChanged(object sender, EventArgs e)
        {
            Task.Factory.StartNew(OnCanExecuteChanged, CancellationToken.None, TaskCreationOptions.None, Scheduler);
        }

        /// <summary>
        /// Raises a CanExecuteChanged event
        /// </summary>
        /// <remarks>Note that this function does not schedule the event</remarks>
        protected virtual void OnCanExecuteChanged()
        {
            var canExecuteChanged = CanExecuteChanged;

            if (canExecuteChanged != null)
            {
                canExecuteChanged(this, EventArgs.Empty);
            }
        }

        #region ICommand Members

        /// <inheritdoc/>
        public event EventHandler CanExecuteChanged = null;

        /// <inheritdoc/>
        public bool CanExecute(object parameter)
        {
            return PortableCommand.CanExecute(parameter);
        }

        /// <inheritdoc/>
        public void Execute(object parameter)
        {
            PortableCommand.Execute(parameter);
        }

        #endregion
    }
}
