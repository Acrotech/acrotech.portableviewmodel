function Log([string]$message)
{
	Write-Host
	Write-Host "##myget[message text='$message']"
}

function OnError([string]$message, [int]$code)
{
	Write-Host
	Write-Host "##myget[buildProblem description='$message ($code)']"

	# currently MyGet does not end the build immediately, so we need to explicity call exit
	exit $code
}

Log "[NuBuild] MyGet Build Started..."

& "$env:GitVersion" /output json

# this will allow us to use the build number instead of the build metadata in the version
$properNuGetVersion = $env:GitVersion_NuGetVersion -replace "(\w)\d*$", [string]::Format("{0}{1:D4}", '${1}', [int]::Parse($env:BuildCounter))
if ($env:GitVersion_NuGetVersion -ne $properNuGetVersion)
{
    Log "[NuBuild] Updating NuGetVersion ($env:GitVersion_NuGetVersion => $properNuGetVersion)"
    $env:GitVersion_NuGetVersion = $properNuGetVersion
}

# Setup some environment variables (if they don't already exist)
if (!$env:NuGetExe) { $env:NuGetExe = $env:NuGet }
if (!$env:VsTestConsoleExe) { $env:VsTestConsoleExe = $env:VsTestConsole }
if (!$env:NuBuildRootPath) { $env:NuBuildRootPath = (Join-Path (Split-Path -Path $MyInvocation.MyCommand.Path) "..") }

& (Join-Path $env:NuBuildRootPath "Build.ps1")
