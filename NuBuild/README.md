﻿# README #

## Status ##

| Branch  | MyGet Build | MyGet Feed | MyGet Downloads | NuGet Feed | NuGet Downloads |
|---------|-------------|------------|-----------------|------------|-----------------|
| develop | [![MyGet Build](https://www.myget.org/BuildSource/Badge/acrotech-develop?identifier=17546033-f1bd-489d-acee-58d130f02a1b)](https://www.myget.org/gallery/acrotech-develop) | [![MyGet Feed](https://img.shields.io/myget/acrotech-develop/vpre/Acrotech.NuBuild.svg?style=flat-square)](https://www.myget.org/gallery/acrotech-develop) | [![MyGet Downloads](https://img.shields.io/myget/acrotech-develop/dt/Acrotech.NuBuild.svg?style=flat-square)](https://www.myget.org/gallery/acrotech-develop) | [![NuGet Feed](https://img.shields.io/badge/nuget-N%2FA-lightgrey.svg?style=flat-square)](https://www.nuget.org/packages/Acrotech.NuBuild) | [![NuGet Downloads](https://img.shields.io/badge/nuget-N%2FA-lightgrey.svg?style=flat-square)](https://www.nuget.org/packages/Acrotech.NuBuild) |
| beta    | [![MyGet Build](https://www.myget.org/BuildSource/Badge/acrotech-beta?identifier=b6ee73ca-350c-40c2-a7d6-906c646833cb)](https://www.myget.org/gallery/acrotech-beta)       | [![MyGet Feed](https://img.shields.io/myget/acrotech-beta/vpre/Acrotech.NuBuild.svg?style=flat-square)](https://www.myget.org/gallery/acrotech-beta)       | [![MyGet Downloads](https://img.shields.io/myget/acrotech-beta/dt/Acrotech.NuBuild.svg?style=flat-square)](https://www.myget.org/gallery/acrotech-beta)       | [![NuGet Feed](https://img.shields.io/nuget/vpre/Acrotech.NuBuild.svg?style=flat-square)](https://www.nuget.org/packages/Acrotech.NuBuild) | [![NuGet Downloads](https://img.shields.io/badge/nuget-N%2FA-lightgrey.svg?style=flat-square)](https://www.nuget.org/packages/Acrotech.NuBuild) |
| master  | [![MyGet Build](https://www.myget.org/BuildSource/Badge/acrotech?identifier=a867fc10-5177-4f17-876b-909283316b91)](https://www.myget.org/gallery/acrotech)                 | [![MyGet Feed](https://img.shields.io/myget/acrotech/v/Acrotech.NuBuild.svg?style=flat-square)](https://www.myget.org/gallery/acrotech)                    | [![MyGet Downloads](https://img.shields.io/myget/acrotech/dt/Acrotech.NuBuild.svg?style=flat-square)](https://www.myget.org/gallery/acrotech)                 | [![NuGet Feed](https://img.shields.io/nuget/v/Acrotech.NuBuild.svg?style=flat-square)](https://www.nuget.org/packages/Acrotech.NuBuild)    | [![NuGet Downloads](https://img.shields.io/nuget/dt/Acrotech.NuBuild.svg?style=flat-square)](https://www.nuget.org/packages/Acrotech.NuBuild)   |


## About ##

**Acrotech.NuBuild** is a set of scripts that simplifies creating NuGet packages (`*.npukg`) both manually and through automated build services. The build scripts are installed into the local solution structure using a NuGet Package Manager Console command (`Install-NuBuild`). This command simply copies the template build files into the solution directory and overwrites any existing NuBuild files. The files in the NuBuild directory, under normal circumstances, should never require modification. All customized build operations are performed by creating any number of build script customization files `NuBuild.ps1` (or `NuBuild.bat`) scripts within the project structure. These scripts are discovered by the NuBuild system and executed after the scripted build environment is setup.

The scripted build environment provides some functions for the build customization files to make use of, these functions are listed below with a short description. The scripted build environment also makes use of some environment variables to customize the build, these variables are listed below with a short description.

Manual builds are started by running the `NuBuild\Manual\Build.bat` script. This batch script will run the `Build.ps1` powershell script in the same directory, bypassing the execution policy which should allow non-elevated users to run the build script. Automated builds are run using specially named script files in the folder named for the build service. These service scripts will pre-initialize the scripted build environment and then call the common build script.

This scripted build system is designed to work in tandem with **Acrotech.Versioning**, which provides a source controllable method of performing app versioning that is accessible both to the build system and the application. However, **Acrotech.Versioning** is optional, its absence will simply require the environment variables or explicit setting of the version.

***NOTE***: When running a manual build, you must either set the `$env:OverrideVersion` to the desired version, or set the version explicitly in `*.nuspec` file, or use **Acrotech.Versioning** to provide a meaningful version.

### Powershell vs Batch Scripts ###

This environment is set up to use powershell scripts, but allows the use of batch scripts. However, when using batch scripts there will be no access to the scripted build environment functions, only the environment variables. Therefore, it is discouraged to use batch scripts except the cases where simple scripts are supplied or powershell is not an option.

### Scripted Build Environment Functions ###

```powershell
# Utility Functions
function Log([string]$message)
function OnError([string]$message, [int]$code)
function Log-Build([string]$message)
function OnError-Exit([string]$message)
function Get-ValueOrDefault([string]$value, [string]$defaultValue = "")
function Get-FormatOrDefault([string]$format, [string]$value, [string]$defaultValue = "")
function Get-Flag([string]$flag)
function Get-Path([string]$relativePath)

# Command Runner Functions
function Add-ParamValue([string[]]$params, [string]$key, [string]$value)
function Add-ParamFormat([string[]]$params, [string]$format, [string]$value)
function Add-Param([string[]]$params, [string]$value)
function Add-ParamList([string[]]$params, [string]$value)
function Run-Command([string]$cmd, [string[]]$params, [string]$onFailedMessage = "Command Failed")

# Standard Build Functions
function Prep-Project(
  [string]$basePath,
  [string]$buildPath = $env:DefaultBuildPropertiesPath,
  [string]$versionPath = $env:DefaultVersionPropertiesPath
)
function Build(
  [string]$basePath,
  [string]$projectFileName,
  [string]$packagesFileName = $env:DefaultPackagesFile,
  [string]$restoreArgs = $env:DefaultNuGetRestoreArgs,
  [string]$versionDetailsPath = $env:DefaultVersionDetailsPath,
  [string]$transformArgs = $env:DefaultTransformArgs,
  [string]$buildArgs = $env:DefaultBuildArgs
)
function Restore-Packages(
  [string]$packagesPath,
  [string]$solutionDirectory = $env:SourcesPath,
  [string]$restoreArgs = $env:DefaultNuGetRestoreArgs
)
function Transform-Project-Version(
  [string]$basePath,
  [string]$versionDetailsPath = $env:DefaultVersionDetailsPath,
  [string]$transformArgs = $env:DefaultTransformArgs
)
function Build-Project(
  [string]$projectPath,
  [string]$buildArgs = $env:DefaultBuildArgs
)
function Run-Tests(
  [string]$assemblyPath,
  [string]$settingsPath = $null,
  [string]$testArgs = $env:DefaultTestArgs
)
function Pack-Packagable(
  [string]$packagablePath,
  [string]$packArgs = $env:DefaultNuGetPackArgs
)

# Internal Build Functions
function Initialize-Environment()
function NuBuild-Discover([string]$scriptPattern = "^NuBuild(\.\w*[0-9]+)?\.(ps1|bat|cmd)$")
function NuBuild-Inject()
```

#### Utility Functions ####

* `Log`: Logs `$message` to the build system.
* `OnError`: Logs the `$message` and `$code` to the build system and ends the build immediately.
* `Log-Build`: Logs `$message` in a formatted string (`[NuBuild] $message`) to the build system. This function should be used for all logging from build customization scripts.
* `OnError-Exit`: Checks the value of `$LASTEXITCODE` and calls `OnError $message $LASTEXITCODE` if necessary. This function should be called if any external commands are called (i.e., using the ampersand operator)
* `Get-ValueOrDefault`: Returns `$value`, or `$defaultValue` if `$value` is null or empty.
* `Get-FormatOrDefault`: Returns `$format` formatted with `$value`, or `$defaultValue` if `$value` is null or empty.
* `Get-Flag`: Returns true if `$flag` is non-empty, false otherwise. This should be used to check the value of any build flags.
* `Get-Path`: returns a fully qualified path using the provided `$relativePath` and joining it to the `$env:SourcesPath`. This should be used to create any required paths for the build functions.

#### Command Runner Functions ####

***NOTE***: All *Add-Param...* functions return a param list. This list may be the same reference that was passed in (if no new params were added), or it could be a new array instance if any new params were added.

* `Add-ParamValue`: Adds both `$key` and `$value` to `$params` if `$value` is non-empty. This is for adding param pairs, i.e., `-Flag True`
* `Add-ParamFormat`: Adds `$format` formatted with `$value` to `$params` if `$value` is non-empty. This is for adding a formatted param such as `-Flag={0}` or `-Set Flag={0}`. Note that the format is split on the space character and each component is formatted with the `$value` then added to the `$params` list.
* `Add-Param`: Adds `$value` to `$params` if `$value` is non-empty. This is for adding a simple parameter, i.e., `-FlagOn`.
* `Add-ParamList`: Adds the list of parameters obtained by splitting `$value` on the space character. Each component is then added to the `$params` list.
* `Run-Command`: Runs the provided `$cmd` (path) using the provided `$params` list and handles any errors that occur using the provided `$onFailedMessage`.

#### Standard Build Functions ####

* `Prep-Project`: Prepares the `build.properties` file (from **Acrotech.Versioning**) if it exists by writing the appropriate build properties to the file (Branch, Commit, Number, Tag, MetaData). Can also prepare the `version.properties` file (if `$env:EnableVersionPropertiesPrep` flag is set). Relative paths of both files may be overridden using `$buildPath` and `$versionPath`.
* `Build`: Composite build function that calls `Restore-Packages`, `Transform-Project-Version`, and `Build-Project` appropriately (with the corresponding parameters provided). See individual functions for more details on the parameters.
* `Restore-Packages`: Calls **NuGet restore** on the `$packagesPath` (NuGet's `packages.config` file). If the `packages.config` file does not exist, then this function is skipped. The `$solutionDirectory` will default to the `$env:SourcesPath`, set this to `$null` when performing a solution restore. Override the default args passed to the restore operation with `$restoreArgs`.
* `Transform-Project-Version`: Transforms the version details T4 template using the relative path specified in `$versionDetailsPath`. If the `$versionDetailsPath` file does not exist, then this function is skipped. Provide additional args to the transform command using `$transformArgs`.
* `Build-Project`: Builds the provided `$projectPath` using MSBuild. If the `$projectPath` file does not exist, then this function is skipped. Provide additional args to MSBuild using `$buildArgs`.
* `Run-Tests`: Runs `VsTest.Console.exe` on the provided `$assemblyPath`. If the `$assemblyPath` does not exist, then this function is skipped. Additionally specify the `$settingsPath` (`*.runsettings` file) to further customize the testing. Override the `VsTest.Console.exe` args using `$testArgs`.
* `Pack-Packagable`: Packs the provided `$packagablePath` using NuGet. If the `$packagablePath` does not exist, then this function is skipped. A packagable may be a nuspec file or a project file (i.e., `*.csproj`). Override the pack args using `$packArgs`.

#### Internal Build Functions ####

* `Initialize-Environment`: Initializes the scripted build environment. This is called automatically at the beginning of a build and should not be called again under normal circumstances. This function performs a NuGet solution restore if a .nuget folder is present.
* `NuBuild-Discover`: Called automatically by the build system to discover build customization scripts and run them. This function can be called again to discover non-standard script names. `$scriptPattern` allows overriding the pattern that matches available script names. The default pattern is `^NuBuild(\.[\w-]*[0-9]+)?\.(ps1|bat|cmd)$` which matches files like `NuBuild.ps1` and `NuBuild.Stage01-Step02.ps1`. This pattern should allow the best level of customization by default. ***NOTE***: In between `NuBuild` and `ps1`, the text must end with a number or the script will not get discovered. These scripts can also be `bat` or `cmd` extensions.
* `NuBuild-Inject`: Called automatically by the build system to perform additional build steps injected through environment variables. This funciton should not be called again under normal circumstances.

### Scripted Build Environment Variables ###

#### Build Versioning ####

These variables are set by GitVersion automatically when built using an automated build system that supports it.

* `$env:GitVersion_Major`: Major version (detected by branch and tag naming)
* `$env:GitVersion_Minor`: Minor version (detected by branch and tag naming)
* `$env:GitVersion_Patch`: Patch version (detected by branch and tag naming)
* `$env:GitVersion_BranchName`: Git branch name.
* `$env:GitVersion_Sha`: Git commit hash.
* `$env:BuildCounter`: Build system build count.
* `$env:GitVersion_PreReleaseTag`: SemVer release tag.
* `$env:GitVersion_BuildMetaData`: SemVer build metadata.
* `$env:GitVersion_NuGetVersion`: NuGet compatible version.

#### Build System ####

These variables are used by the scripted build environment to execute and customize the build.

* `$env:NuBuildRootPath`: Directory that contains the NuBuild files (typically found in `$SourcesPath\NuBuild`). This is usually auto discovered by the initial running scripts.
* `$env:SourcesPath`: Root directory of the build, i.e., where source files are checked out. This is usually auto discovered relative to `$env:NuBuildRootPath`.
* `$env:VisualStudioVersion`: Visual Studio version number. This defaults to `12.0`.

* `$env:MsBuildExe`: Path to the MSBuild executable.
* `$env:TextTransformExe`: Path to the TextTransform executable.
* `$env:VsTestConsoleExe`: Path to the VsTest.Console executable.
* `$env:NuGetExe`: Path to the NuGet executable.

* `$env:NuGetOutDir`: Directory where `*.nupkg` files are written to. This defaults to `$env:NuBuildRootPath\nupkg`.
* `$env:Targets`: MSBuild targets. This defaults to `Rebuild`.
* `$env:Configuration`: MSBuild configuration. This defaults to `Release`.
* `$env:Platform`: MSBuild platform. This defaults to `AnyCPU`. ***NOTE***: This must be updated to `Any CPU` to build a solution file.
* `$env:OverrideVersion`: When non-empty, this version will be used in the NuGet pack command to override the version that would otherwise be used.

* `$env:DefaultNuGetRestoreArgs`: NuGet restore args. This defaults to `-NoCache -NonInteractive`.
* `$env:DefaultNuGetVerbosity`: Verbosity of the NuGet commands. This defaults to `Detailed`.
* `$env:DefaultBuildPropertiesPath`: `build.properties` relative path. This defaults to `Properties\build.properties`.
* `$env:DefaultVersionPropertiesPath`: `version.properties` relative path. This defaults to `Properties\version.properties`.
* `$env:DefaultVersionDetailsPath`: `VersionDetails.tt` relative path. This defaults to `Properties\VersionDetails.tt`.
* `$env:DefaultPackagesFile`: `packages.config` file name. This defaults to `packages.config`.
* `$env:DefaultTransformArgs`: TextTransform args. This default is empty.
* `$env:DefaultBuildArgs`: MSBuild args. This default is empty.
* `$env:DefaultTestArgs`: VsTest.Console args. This defaults to `/InIsolation /Enablecodecoverage`.
* `$env:DefaultNuGetPackArgs`: NuGet pack args. This defaults to `-NonInteractive -NoPackageAnalysis -IncludeReferencedProjects`. ***NOTE***: Append `-Source <my source>` to this to use a custom package source.

* `$env:EnableGitVersionOverride`: When non-empty, this will automatically set `$env:OverrideVersion` to `$env:GitVersion_NuGetVersion` after build initialization.
* `$env:EnableVersionPropertiesPrep`: When non-empty, this will automatically update the `version.properties` file using the `$env:GitVersion_Major`, `$env:GitVersion_Minor`, and `$env:GitVersion_Patch` values.

#### Build Injections ####

These variables allow build functions to be called by injecting paths into the build. The variable is split by comma and the resulting values are sent to their respective build functions.

* `$env:PrepIncludes`: CSV of paths to send to `Prep-Project`
* `$env:RestoreIncludes`: CSV of paths to send to `Restore-Packages`
* `$env:TransformIncludes`: CSV of paths to send to `Transform-Project-Version`
* `$env:BuildIncludes`: CSV of paths to send to `Build-Project`
* `$env:TestIncludes`: CSV of paths to send to `Run-Tests`
* `$env:PackIncludes`: CSV of paths to send to `Pack-Packagable`

### Supported Build Services ###

#### MyGet ####

[MyGet Build Services](http://www.myget.org) provides free NuGet build services to open source projects (like this one!). The scripts run within a GitVersion environment and automatically adjust the `$env:GitVersion_NuGetVersion` to use MyGet's build counter rather than the `$env:GitVersion_BuildMetaData`.

## Instructions ##

NuBuild is incredibly simple to set up, it only requires installing the scripts and creating one or more customization scripts.

0. After installing this NuGet package (or updating to a newer version), open the Package Manager Console and run `Install-NuBuild`. This will copy the NuBuild template files into your solution folder (or update the existing files if you have previously installed NuBuild).
0. Create a new script named `NuBuild.ps1` in your project directory (or in the solution directory if that makes more sense for your build) and customize it to your needs. ***NOTE***: there is a template NuBuild script you can use as a reference (`NuBuild\NuBuild.Template.ps1`).

That's it, your project is now ready to build on MyGet!

***NOTE***: if you aren't using **Acrotech.Versioning** you will probably want to set `$env:EnableGitVersionOverride` to `TRUE` (this will automatically use the discovered `$env:GitVersion_NuGetVersion`)

## Changelog ##

#### 1.0.0 ####

* Initial Release
* Support for MyGet Build Services
